## OpenHarmony_4.0.6.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.6.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.6.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-04-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420_091051/version-Master_Version-OpenHarmony_4.0.6.3-20230420_091051-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420_091058/version-Master_Version-OpenHarmony_4.0.6.3-20230420_091058-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420_091128/version-Master_Version-OpenHarmony_4.0.6.3-20230420_091128-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-04-20**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420_071031/version-Master_Version-OpenHarmony_4.0.6.3-20230420_071031-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420_091056/version-Master_Version-OpenHarmony_4.0.6.3-20230420_091056-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420_091139/version-Master_Version-OpenHarmony_4.0.6.3-20230420_091139-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420045751/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420075849/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420075450/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.3/20230420050347/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.6.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.6.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.6.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-04-13**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230413_141040/version-Master_Version-OpenHarmony_4.0.6.2-20230413_141040-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230413_162739/version-Master_Version-OpenHarmony_4.0.6.2-20230413_162739-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230413_162754/version-Master_Version-OpenHarmony_4.0.6.2-20230413_162754-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-04-13**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230413_141106/version-Master_Version-OpenHarmony_4.0.6.2-20230413_141106-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230412_191103/version-Master_Version-OpenHarmony_4.0.6.2-20230412_191103-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230412_191008/version-Master_Version-OpenHarmony_4.0.6.2-20230412_191008-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230412201035/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230412205050/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230412200629/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.2/20230412210955/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.6.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.6.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.6.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-04-06**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230406_191021/version-Master_Version-OpenHarmony_4.0.6.1-20230406_191021-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230406_141014/version-Master_Version-OpenHarmony_4.0.6.1-20230406_141014-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230406_141037/version-Master_Version-OpenHarmony_4.0.6.1-20230406_141037-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-04-06**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230406_141111/version-Master_Version-OpenHarmony_4.0.6.1-20230406_141111-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230408_091539/version-Master_Version-OpenHarmony_4.0.6.1-20230408_091539-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230408_091029/version-Master_Version-OpenHarmony_4.0.6.1-20230408_091029-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230408090636/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230408075600/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230408074958/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.6.1/20230408045930/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.5(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.5(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.5(sp5):  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-31**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331_170406/version-Master_Version-OpenHarmony_4.0.5.5-20230331_170406-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331_170429/version-Master_Version-OpenHarmony_4.0.5.5-20230331_170429-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331_170504/version-Master_Version-OpenHarmony_4.0.5.5-20230331_170504-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-31**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331_170601/version-Master_Version-OpenHarmony_4.0.5.5-20230331_170601-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331_173536/version-Master_Version-OpenHarmony_4.0.5.5-20230331_173536-ohos-sdk.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331_173603/version-Master_Version-OpenHarmony_4.0.5.5-20230331_173603-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331183556/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331182331/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331192715/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230331183934/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.5(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.5(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.5(sp3):  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-29**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329_110312/version-Master_Version-OpenHarmony_4.0.5.5-20230329_110312-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329_110311/version-Master_Version-OpenHarmony_4.0.5.5-20230329_110311-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329_110251/version-Master_Version-OpenHarmony_4.0.5.5-20230329_110251-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-29**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329_110349/version-Master_Version-OpenHarmony_4.0.5.5-20230329_110349-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329_110318/version-Master_Version-OpenHarmony_4.0.5.5-20230329_110318-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329_110414/version-Master_Version-OpenHarmony_4.0.5.5-20230329_110414-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329115612/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329131457/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329114849/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230329125804/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.5(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.5(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.5(sp2):  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327_164624/version-Master_Version-OpenHarmony_4.0.5.5-20230327_164624-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327_164734/version-Master_Version-OpenHarmony_4.0.5.5-20230327_164734-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327_164735/version-Master_Version-OpenHarmony_4.0.5.5-20230327_164735-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327_164836/version-Master_Version-OpenHarmony_4.0.5.5-20230327_164836-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327_164903/version-Master_Version-OpenHarmony_4.0.5.5-20230327_164903-ohos-sdk.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327_164938/version-Master_Version-OpenHarmony_4.0.5.5-20230327_164938-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327173909/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327180041/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327182247/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230327173601/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.5(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.5(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.5(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-24**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324_211552/version-Master_Version-OpenHarmony_4.0.5.5-20230324_211552-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324_211628/version-Master_Version-OpenHarmony_4.0.5.5-20230324_211628-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324_211759/version-Master_Version-OpenHarmony_4.0.5.5-20230324_211759-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-24**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324_211841/version-Master_Version-OpenHarmony_4.0.5.5-20230324_211841-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324_213625/version-Master_Version-OpenHarmony_4.0.5.5-20230324_213625-ohos-sdk.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324_213632/version-Master_Version-OpenHarmony_4.0.5.5-20230324_213632-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324225528/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324231846/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324224119/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230324232909/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-22**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322_181005/version-Master_Version-OpenHarmony_4.0.5.5-20230322_181005-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322_181113/version-Master_Version-OpenHarmony_4.0.5.5-20230322_181113-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322_181148/version-Master_Version-OpenHarmony_4.0.5.5-20230322_181148-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-22**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322_180551/version-Master_Version-OpenHarmony_4.0.5.5-20230322_180551-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322_191015/version-Master_Version-OpenHarmony_4.0.5.5-20230322_191015-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322_180758/version-Master_Version-OpenHarmony_4.0.5.5-20230322_180758-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322192349/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322190230/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230322185327/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.5/20230321181837/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316_144528/version-Master_Version-OpenHarmony_4.0.5.3-20230316_144528-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316_144616/version-Master_Version-OpenHarmony_4.0.5.3-20230316_144616-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316_144614/version-Master_Version-OpenHarmony_4.0.5.3-20230316_144614-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-16**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316_141100/version-Master_Version-OpenHarmony_4.0.5.3-20230316_141100-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316_144355/version-Master_Version-OpenHarmony_4.0.5.3-20230316_144355-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316_144433/version-Master_Version-OpenHarmony_4.0.5.3-20230316_144433-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316161429/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316152710/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316151102/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.3/20230316165802/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-09**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309_191036/version-Master_Version-OpenHarmony_4.0.5.2-20230309_191036-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309_185309/version-Master_Version-OpenHarmony_4.0.5.2-20230309_185309-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309_190051/version-Master_Version-OpenHarmony_4.0.5.2-20230309_190051-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-09**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309_182850/version-Master_Version-OpenHarmony_4.0.5.2-20230309_182850-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309_151802/version-Master_Version-OpenHarmony_4.0.5.2-20230309_151802-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309_151827/version-Master_Version-OpenHarmony_4.0.5.2-20230309_151827-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309172144/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309181840/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309173405/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.2/20230309192152/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.10版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.10 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.10版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-03**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.10/20230303_145352/version-Daily_Version-OpenHarmony_3.2.10.10-20230303_145352-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.10/20230303_145444/version-Daily_Version-OpenHarmony_3.2.10.10-20230303_145444-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.10/20230303_145530/version-Daily_Version-OpenHarmony_3.2.10.10-20230303_145530-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-03**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.10/20230303_145027/version-Daily_Version-OpenHarmony_3.2.10.10-20230303_145027-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.10/20230303_145524/version-Daily_Version-OpenHarmony_3.2.10.10-20230303_145524-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.10/20230303_145552/version-Daily_Version-OpenHarmony_3.2.10.10-20230303_145552-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.10/20230303154256/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.10/20230303154207/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.10/20230303161721/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.10/20230303153601/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.5.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.5.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.5.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-03-01**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.1/20230301_140947/version-Master_Version-OpenHarmony_4.0.5.1-20230301_140947-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.1/20230301_090948/version-Master_Version-OpenHarmony_4.0.5.1-20230301_090948-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.1/20230301_091003/version-Master_Version-OpenHarmony_4.0.5.1-20230301_091003-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-03-01**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.1/20230301_071049/version-Master_Version-OpenHarmony_4.0.5.1-20230301_071049-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.1/20230301_191025/version-Master_Version-OpenHarmony_4.0.5.1-20230301_191025-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.5.1/20230301_173537/version-Master_Version-OpenHarmony_4.0.5.1-20230301_173537-ohos-sdk-public.tar.gz |

## OpenHarmony_3.2.10.9版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.9 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.9版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-25**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225_073747/version-Master_Version-OpenHarmony_3.2.10.9-20230225_073747-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225_073742/version-Master_Version-OpenHarmony_3.2.10.9-20230225_073742-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225_074328/version-Master_Version-OpenHarmony_3.2.10.9-20230225_074328-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-25**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230224_170455/version-Master_Version-OpenHarmony_3.2.10.9-20230224_170455-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225_073754/version-Master_Version-OpenHarmony_3.2.10.9-20230225_073754-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225_073913/version-Master_Version-OpenHarmony_3.2.10.9-20230225_073913-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225024850/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225014405/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225053910/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.9/20230225063844/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.3.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.3.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.3.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-23**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.5/20230223_091009/version-Master_Version-OpenHarmony_4.0.3.5-20230223_091009-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.5/20230223_090953/version-Master_Version-OpenHarmony_4.0.3.5-20230223_090953-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.5/20230223_091009/version-Master_Version-OpenHarmony_4.0.3.5-20230223_091009-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-23**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.5/20230223_091005/version-Master_Version-OpenHarmony_4.0.3.5-20230223_091005-dayu200.tar.gz |

## OpenHarmony_3.2.10.8版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.8 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.8版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-17**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.8/20230217_095517/version-Daily_Version-OpenHarmony_3.2.10.8-20230217_095517-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217_162816/version-Master_Version-OpenHarmony_3.2.10.8-20230217_162816-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217_162749/version-Master_Version-OpenHarmony_3.2.10.8-20230217_162749-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-17**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.8/20230217_095703/version-Daily_Version-OpenHarmony_3.2.10.8-20230217_095703-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217_164456/version-Master_Version-OpenHarmony_3.2.10.8-20230217_164456-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217_163816/version-Master_Version-OpenHarmony_3.2.10.8-20230217_163816-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217171836/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217170431/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217191058/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.8/20230217195553/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.3.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.3.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.3.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216_145812/version-Master_Version-OpenHarmony_4.0.3.3-20230216_145812-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216_031018/version-Master_Version-OpenHarmony_4.0.3.3-20230216_031018-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216_031010/version-Master_Version-OpenHarmony_4.0.3.3-20230216_031010-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-16**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216_071015/version-Master_Version-OpenHarmony_4.0.3.3-20230216_071015-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216_031326/version-Master_Version-OpenHarmony_4.0.3.3-20230216_031326-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216_031321/version-Master_Version-OpenHarmony_4.0.3.3-20230216_031321-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216044259/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216074335/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216074041/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.3/20230216045100/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.3.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.3.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.3.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-11**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211_091053/version-Master_Version-OpenHarmony_4.0.3.2-20230211_091053-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211_091003/version-Master_Version-OpenHarmony_4.0.3.2-20230211_091003-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211_091026/version-Master_Version-OpenHarmony_4.0.3.2-20230211_091026-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-11**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211_071031/version-Master_Version-OpenHarmony_4.0.3.2-20230211_071031-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211_091103/version-Master_Version-OpenHarmony_4.0.3.2-20230211_091103-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211_091025/version-Master_Version-OpenHarmony_4.0.3.2-20230211_091025-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211044155/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211074226/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211073850/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.2/20230211044807/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.6(sp6)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.6(sp6) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.6(sp6)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-11**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211_073817/version-Master_Version-OpenHarmony_3.2.10.6-20230211_073817-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211_103757/version-Master_Version-OpenHarmony_3.2.10.6-20230211_103757-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211_103748/version-Master_Version-OpenHarmony_3.2.10.6-20230211_103748-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-11**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211_074450/version-Master_Version-OpenHarmony_3.2.10.6-20230211_074450-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211_073741/version-Master_Version-OpenHarmony_3.2.10.6-20230211_073741-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211_074412/version-Master_Version-OpenHarmony_3.2.10.6-20230211_074412-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211024239/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211014204/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211053819/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230211063837/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.6(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.6(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.6(sp5)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-08**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.6/20230208_145412/version-Daily_Version-OpenHarmony_3.2.10.6-20230208_145412-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.6/20230208_145410/version-Daily_Version-OpenHarmony_3.2.10.6-20230208_145410-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.6/20230208_145421/version-Daily_Version-OpenHarmony_3.2.10.6-20230208_145421-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-08**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.6/20230208_155945/version-Daily_Version-OpenHarmony_3.2.10.6-20230208_155945-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230208_163837/version-Master_Version-OpenHarmony_3.2.10.6-20230208_163837-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230208_163836/version-Master_Version-OpenHarmony_3.2.10.6-20230208_163836-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230208153515/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230208153751/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230208161319/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230208153152/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.6(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.6(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.6(sp3)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-06**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206_073822/version-Master_Version-OpenHarmony_3.2.10.6-20230206_073822-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206_103845/version-Master_Version-OpenHarmony_3.2.10.6-20230206_103845-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206_103808/version-Master_Version-OpenHarmony_3.2.10.6-20230206_103808-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-06**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206_103815/version-Master_Version-OpenHarmony_3.2.10.6-20230206_103815-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206_103815/version-Master_Version-OpenHarmony_3.2.10.6-20230206_103815-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206_104352/version-Master_Version-OpenHarmony_3.2.10.6-20230206_104352-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206024059/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206014113/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206053811/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230206063816/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.3.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.3.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.3.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-02-02**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202_140719/version-Master_Version-OpenHarmony_4.0.3.1-20230202_140719-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202_140806/version-Master_Version-OpenHarmony_4.0.3.1-20230202_140806-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202_140914/version-Master_Version-OpenHarmony_4.0.3.1-20230202_140914-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-02-02**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202_141014/version-Master_Version-OpenHarmony_4.0.3.1-20230202_141014-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202_140935/version-Master_Version-OpenHarmony_4.0.3.1-20230202_140935-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202_141046/version-Master_Version-OpenHarmony_4.0.3.1-20230202_141046-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202160816/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202145314/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202144721/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.3.1/20230202160208/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2 Beta5 版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2 Beta5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2 Beta5 版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-31**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131_201753/version-Master_Version-OpenHarmony_3.2.10.6-20230131_201753-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131_183116/version-Master_Version-OpenHarmony_3.2.10.6-20230131_183116-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131_183144/version-Master_Version-OpenHarmony_3.2.10.6-20230131_183144-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-31**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131_192722/version-Master_Version-OpenHarmony_3.2.10.6-20230131_192722-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131_183209/version-Master_Version-OpenHarmony_3.2.10.6-20230131_183209-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131_183333/version-Master_Version-OpenHarmony_3.2.10.6-20230131_183333-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131180344/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131195400/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131195109/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230131175442/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.6(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.6(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.6(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130_074350/version-Master_Version-OpenHarmony_3.2.10.6-20230130_074350-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130_074408/version-Master_Version-OpenHarmony_3.2.10.6-20230130_074408-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130_074405/version-Master_Version-OpenHarmony_3.2.10.6-20230130_074405-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-30**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Daily_Version/dayu200/20230130_094739/version-Daily_Version-dayu200-20230130_094739-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.6/20230130_175251/version-Daily_Version-OpenHarmony_3.2.10.6-20230130_175251-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.10.6/20230130_175247/version-Daily_Version-OpenHarmony_3.2.10.6-20230130_175247-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130183635/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130183448/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130191036/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230130183049/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.6版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.6 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.6版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-29**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129_074355/version-Master_Version-OpenHarmony_3.2.10.6-20230129_074355-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129_073827/version-Master_Version-OpenHarmony_3.2.10.6-20230129_073827-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129_074141/version-Master_Version-OpenHarmony_3.2.10.6-20230129_074141-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-29**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129_074441/version-Master_Version-OpenHarmony_3.2.10.6-20230129_074441-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129_073827/version-Master_Version-OpenHarmony_3.2.10.6-20230129_073827-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129_073825/version-Master_Version-OpenHarmony_3.2.10.6-20230129_073825-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129024454/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.6/20230129014323/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_4.0.2.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.2.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.2.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-18**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118_182232/version-Master_Version-OpenHarmony_4.0.2.3-20230118_182232-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118_182202/version-Master_Version-OpenHarmony_4.0.2.3-20230118_182202-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118_182243/version-Master_Version-OpenHarmony_4.0.2.3-20230118_182243-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-18**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118_182415/version-Master_Version-OpenHarmony_4.0.2.3-20230118_182415-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118_171030/version-Master_Version-OpenHarmony_4.0.2.3-20230118_171030-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118_171035/version-Master_Version-OpenHarmony_4.0.2.3-20230118_171035-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118192407/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118201131/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118200416/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.3/20230118191908/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-17**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117_084417/version-Master_Version-OpenHarmony_3.2.10.5-20230117_084417-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117_073904/version-Master_Version-OpenHarmony_3.2.10.5-20230117_073904-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117_073902/version-Master_Version-OpenHarmony_3.2.10.5-20230117_073902-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-17**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117_073844/version-Master_Version-OpenHarmony_3.2.10.5-20230117_073844-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117_073904/version-Master_Version-OpenHarmony_3.2.10.5-20230117_073904-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117_073839/version-Master_Version-OpenHarmony_3.2.10.5-20230117_073839-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117024426/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117015106/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117053826/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.5/20230117063837/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.2.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.2.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.2.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-12**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112_111021/version-Master_Version-OpenHarmony_4.0.2.2-20230112_111021-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112_071019/version-Master_Version-OpenHarmony_4.0.2.2-20230112_071019-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112_071039/version-Master_Version-OpenHarmony_4.0.2.2-20230112_071039-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-12**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112_071050/version-Master_Version-OpenHarmony_4.0.2.2-20230112_071050-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112_071038/version-Master_Version-OpenHarmony_4.0.2.2-20230112_071038-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112_071018/version-Master_Version-OpenHarmony_4.0.2.2-20230112_071018-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112044452/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112174904/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112073818/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.2/20230112044827/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-05**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105_164348/version-Master_Version-OpenHarmony_3.2.10.3-20230105_164348-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105_164413/version-Master_Version-OpenHarmony_3.2.10.3-20230105_164413-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105_164406/version-Master_Version-OpenHarmony_3.2.10.3-20230105_164406-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-05**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105_164412/version-Master_Version-OpenHarmony_3.2.10.3-20230105_164412-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105_163913/version-Master_Version-OpenHarmony_3.2.10.3-20230105_163913-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105_163920/version-Master_Version-OpenHarmony_3.2.10.3-20230105_163920-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105024255/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105014245/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105053930/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.3/20230105063816/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.2.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.2.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.2.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2023-01-05**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105_151002/version-Master_Version-OpenHarmony_4.0.2.1-20230105_151002-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105_151021/version-Master_Version-OpenHarmony_4.0.2.1-20230105_151021-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105_151104/version-Master_Version-OpenHarmony_4.0.2.1-20230105_151104-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2023-01-05**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105_131042/version-Master_Version-OpenHarmony_4.0.2.1-20230105_131042-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105_151036/version-Master_Version-OpenHarmony_4.0.2.1-20230105_151036-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105_151045/version-Master_Version-OpenHarmony_4.0.2.1-20230105_151045-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105153149/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105161823/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105152826/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.2.1/20230105161358/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.1.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.1.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.1.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-29**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229_161026/version-Master_Version-OpenHarmony_4.0.1.5-20221229_161026-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229_161027/version-Master_Version-OpenHarmony_4.0.1.5-20221229_161027-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229_161040/version-Master_Version-OpenHarmony_4.0.1.5-20221229_161040-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-29**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229_165812/version-Master_Version-OpenHarmony_4.0.1.5-20221229_165812-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229_161016/version-Master_Version-OpenHarmony_4.0.1.5-20221229_161016-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229_161031/version-Master_Version-OpenHarmony_4.0.1.5-20221229_161031-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229173605/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229173651/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229205217/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.5/20221229190503/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227_080824/version-Master_Version-OpenHarmony_3.2.10.2-20221227_080824-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227_080841/version-Master_Version-OpenHarmony_3.2.10.2-20221227_080841-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227_080830/version-Master_Version-OpenHarmony_3.2.10.2-20221227_080830-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227_020745/version-Master_Version-OpenHarmony_3.2.10.2-20221227_020745-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227_080826/version-Master_Version-OpenHarmony_3.2.10.2-20221227_080826-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227_080822/version-Master_Version-OpenHarmony_3.2.10.2-20221227_080822-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227024049/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227014055/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227053917/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.2/20221227063849/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.1.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.1.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.1.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-22**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222_071026/version-Master_Version-OpenHarmony_4.0.1.3-20221222_071026-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222_071026/version-Master_Version-OpenHarmony_4.0.1.3-20221222_071026-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222_071047/version-Master_Version-OpenHarmony_4.0.1.3-20221222_071047-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-22**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222_071555/version-Master_Version-OpenHarmony_4.0.1.3-20221222_071555-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222_081709/version-Master_Version-OpenHarmony_4.0.1.3-20221222_081709-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222_081256/version-Master_Version-OpenHarmony_4.0.1.3-20221222_081256-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222044425/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222074526/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222073846/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.3/20221222044814/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.10.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.10.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.10.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220_100751/version-Master_Version-OpenHarmony_3.2.10.1-20221220_100751-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220_164008/version-Master_Version-OpenHarmony_3.2.10.1-20221220_164008-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220_164035/version-Master_Version-OpenHarmony_3.2.10.1-20221220_164035-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-20**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220_165525/version-Master_Version-OpenHarmony_3.2.10.1-20221220_165525-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220_160820/version-Master_Version-OpenHarmony_3.2.10.1-20221220_160820-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220_160816/version-Master_Version-OpenHarmony_3.2.10.1-20221220_160816-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220171754/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220184307/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220053934/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.10.1/20221220171327/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_4.0.1.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.1.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.1.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216_144746/version-Master_Version-OpenHarmony_4.0.1.2-20221216_144746-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216_081006/version-Master_Version-OpenHarmony_4.0.1.2-20221216_081006-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216_081014/version-Master_Version-OpenHarmony_4.0.1.2-20221216_081014-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-16**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216_021311/version-Master_Version-OpenHarmony_4.0.1.2-20221216_021311-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216_081017/version-Master_Version-OpenHarmony_4.0.1.2-20221216_081017-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216_081122/version-Master_Version-OpenHarmony_4.0.1.2-20221216_081122-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216044951/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216075036/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216073803/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_4.0.1.2/20221216044746/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-13**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213_140831/version-Master_Version-OpenHarmony_3.2.9.2-20221213_140831-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213_140840/version-Master_Version-OpenHarmony_3.2.9.2-20221213_140840-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213_140801/version-Master_Version-OpenHarmony_3.2.9.2-20221213_140801-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-13**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213_070737/version-Master_Version-OpenHarmony_3.2.9.2-20221213_070737-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213_140916/version-Master_Version-OpenHarmony_3.2.9.2-20221213_140916-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213_140918/version-Master_Version-OpenHarmony_3.2.9.2-20221213_140918-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213163129/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213172419/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213172124/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221213162322/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp11)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp11) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp11)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-09**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209_160818/version-Master_Version-OpenHarmony_3.2.9.2-20221209_160818-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209_152539/version-Master_Version-OpenHarmony_3.2.9.2-20221209_152539-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209_152439/version-Master_Version-OpenHarmony_3.2.9.2-20221209_152439-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-09**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209_160633/version-Master_Version-OpenHarmony_3.2.9.2-20221209_160633-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209_140801/version-Master_Version-OpenHarmony_3.2.9.2-20221209_140801-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209_140641/version-Master_Version-OpenHarmony_3.2.9.2-20221209_140641-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209161005/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209163203/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209164913/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221209160539/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp10)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp10) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp10)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-08**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208_081253/version-Master_Version-OpenHarmony_3.2.9.2-20221208_081253-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208_080636/version-Master_Version-OpenHarmony_3.2.9.2-20221208_080636-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208_080750/version-Master_Version-OpenHarmony_3.2.9.2-20221208_080750-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-08**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208_080656/version-Master_Version-OpenHarmony_3.2.9.2-20221208_080656-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208_080631/version-Master_Version-OpenHarmony_3.2.9.2-20221208_080631-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208_080817/version-Master_Version-OpenHarmony_3.2.9.2-20221208_080817-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208024148/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208014250/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208053821/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221208063827/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp9)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp9) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp9)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-12-06**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206_175504/version-Master_Version-OpenHarmony_3.2.9.2-20221206_175504-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206_120746/version-Master_Version-OpenHarmony_3.2.9.2-20221206_120746-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206_120739/version-Master_Version-OpenHarmony_3.2.9.2-20221206_120739-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-12-06**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206_120639/version-Master_Version-OpenHarmony_3.2.9.2-20221206_120639-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206_120753/version-Master_Version-OpenHarmony_3.2.9.2-20221206_120753-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206_120640/version-Master_Version-OpenHarmony_3.2.9.2-20221206_120640-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206151456/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206180853/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206180852/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.2/20221206151151/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp7)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp7) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp7)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-28**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141225/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141225-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141334/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141334-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141438/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141438-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-28**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_151304/version-Master_Version-OpenHarmony_3.2.9.1-20221128_151304-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141610/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141610-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141451/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141451-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128145253/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128152055/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128153015/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128145054/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp6)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp6) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp6)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-28**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141225/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141225-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141334/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141334-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141438/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141438-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-28**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_151304/version-Master_Version-OpenHarmony_3.2.9.1-20221128_151304-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141610/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141610-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128_141451/version-Master_Version-OpenHarmony_3.2.9.1-20221128_141451-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128145253/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128152055/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128153015/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221128145054/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp5)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-26**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126_140633/version-Master_Version-OpenHarmony_3.2.9.1-20221126_140633-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126_140634/version-Master_Version-OpenHarmony_3.2.9.1-20221126_140634-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126_140633/version-Master_Version-OpenHarmony_3.2.9.1-20221126_140633-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-26**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126_142839/version-Master_Version-OpenHarmony_3.2.9.1-20221126_142839-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126_140800/version-Master_Version-OpenHarmony_3.2.9.1-20221126_140800-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126_140755/version-Master_Version-OpenHarmony_3.2.9.1-20221126_140755-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Daily_Version/defaultvalue/20221126150559/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/defaultvalue/20221126150316/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Daily_Version/defaultvalue/20221126150051/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221126161107/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp3)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-25**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125_151348/version-Master_Version-OpenHarmony_3.2.9.1-20221125_151348-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125_151244/version-Master_Version-OpenHarmony_3.2.9.1-20221125_151244-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125_151346/version-Master_Version-OpenHarmony_3.2.9.1-20221125_151346-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-25**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125_153507/version-Master_Version-OpenHarmony_3.2.9.1-20221125_153507-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125_151452/version-Master_Version-OpenHarmony_3.2.9.1-20221125_151452-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125_151355/version-Master_Version-OpenHarmony_3.2.9.1-20221125_151355-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125155756/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125162734/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125163254/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221125155248/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp2)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-24**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221124_180632/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221124_180632-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221124_180637/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221124_180637-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221124_180757/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221124_180757-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-24**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221124_181926/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221124_181926-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221124_080803/version-Master_Version-OpenHarmony_3.2.9.1-20221124_080803-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221124_080806/version-Master_Version-OpenHarmony_3.2.9.1-20221124_080806-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221124024346/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221124014405/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221124053756/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221124063653/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-22**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221122_080641/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221122_080641-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221122_080746/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221122_080746-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221122_080747/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221122_080747-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-22**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221122_080643/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221122_080643-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221122_080819/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221122_080819-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1(sp1)/20221122_080807/version-Master_Version-OpenHarmony_3.2.9.1(sp1)-20221122_080807-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221122041216/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221122013932/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221122053627/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221122063622/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.9.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.9.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.9.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-19**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119_080635/version-Master_Version-OpenHarmony_3.2.9.1-20221119_080635-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119_080744/version-Master_Version-OpenHarmony_3.2.9.1-20221119_080744-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119_080638/version-Master_Version-OpenHarmony_3.2.9.1-20221119_080638-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-19**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119_080648/version-Master_Version-OpenHarmony_3.2.9.1-20221119_080648-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119_120758/version-Master_Version-OpenHarmony_3.2.9.1-20221119_120758-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119_120638/version-Master_Version-OpenHarmony_3.2.9.1-20221119_120638-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119122015/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119113040/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119112213/L2-SDK-MAC-M1-PUBLIC.tar.gz |
| mac-m1-full: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221119120055/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.8.3(sp8)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp8) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp8)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-14**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116_140639/version-Master_Version-OpenHarmony_3.2.9.1-20221116_140639-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116_140641/version-Master_Version-OpenHarmony_3.2.9.1-20221116_140641-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116_140633/version-Master_Version-OpenHarmony_3.2.9.1-20221116_140633-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-14**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116_145928/version-Master_Version-OpenHarmony_3.2.9.1-20221116_145928-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116_080745/version-Master_Version-OpenHarmony_3.2.9.1-20221116_080745-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116_080636/version-Master_Version-OpenHarmony_3.2.9.1-20221116_080636-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116023909/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116014105/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-m1-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.9.1/20221116053613/L2-SDK-MAC-M1-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3(sp7)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp7) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp7)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-11**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111_080636/version-Master_Version-OpenHarmony_3.2.8.3-20221111_080636-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111_080631/version-Master_Version-OpenHarmony_3.2.8.3-20221111_080631-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111_080624/version-Master_Version-OpenHarmony_3.2.8.3-20221111_080624-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-11**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111_100712/version-Master_Version-OpenHarmony_3.2.8.3-20221111_100712-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111_080624/version-Master_Version-OpenHarmony_3.2.8.3-20221111_080624-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111_080739/version-Master_Version-OpenHarmony_3.2.8.3-20221111_080739-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111024415/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221111013856/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3(sp6)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp6) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp6)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-10**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110_162757/version-Master_Version-OpenHarmony_3.2.8.3-20221110_162757-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110_152427/version-Master_Version-OpenHarmony_3.2.8.3-20221110_152427-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110_152442/version-Master_Version-OpenHarmony_3.2.8.3-20221110_152442-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-10**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110_152446/version-Master_Version-OpenHarmony_3.2.8.3-20221110_152446-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110_155207/version-Master_Version-OpenHarmony_3.2.8.3-20221110_155207-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110_152458/version-Master_Version-OpenHarmony_3.2.8.3-20221110_152458-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110163112/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221110160724/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp5)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-07**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107_180632/version-Master_Version-OpenHarmony_3.2.8.3-20221107_180632-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107_180631/version-Master_Version-OpenHarmony_3.2.8.3-20221107_180631-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107_180630/version-Master_Version-OpenHarmony_3.2.8.3-20221107_180630-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-07**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107_180633/version-Master_Version-OpenHarmony_3.2.8.3-20221107_180633-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107_180758/version-Master_Version-OpenHarmony_3.2.8.3-20221107_180758-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107_180631/version-Master_Version-OpenHarmony_3.2.8.3-20221107_180631-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107190850/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221107190748/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp3)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-04**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104_100634/version-Master_Version-OpenHarmony_3.2.8.3-20221104_100634-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104_080740/version-Master_Version-OpenHarmony_3.2.8.3-20221104_080740-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104_080630/version-Master_Version-OpenHarmony_3.2.8.3-20221104_080630-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-04**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104_080629/version-Master_Version-OpenHarmony_3.2.8.3-20221104_080629-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104_080819/version-Master_Version-OpenHarmony_3.2.8.3-20221104_080819-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104_080639/version-Master_Version-OpenHarmony_3.2.8.3-20221104_080639-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104023759/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221104013812/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp2)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-11-01**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Daily_Version/hispark_pegasus/20221101_171017/version-Daily_Version-hispark_pegasus-20221101_171017-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101_142418/version-Master_Version-OpenHarmony_3.2.8.3-20221101_142418-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101_142242/version-Master_Version-OpenHarmony_3.2.8.3-20221101_142242-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-11-01**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101_141844/version-Master_Version-OpenHarmony_3.2.8.3-20221101_141844-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101_142331/version-Master_Version-OpenHarmony_3.2.8.3-20221101_142331-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101_142344/version-Master_Version-OpenHarmony_3.2.8.3-20221101_142344-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101144115/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221101153412/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-10-28**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221029_100637/version-Master_Version-OpenHarmony_3.2.8.3-20221029_100637-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221028_192841/version-Master_Version-OpenHarmony_3.2.8.3-20221028_192841-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221028_192854/version-Master_Version-OpenHarmony_3.2.8.3-20221028_192854-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-10-28**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221028_200631/version-Master_Version-OpenHarmony_3.2.8.3-20221028_200631-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221028_193053/version-Master_Version-OpenHarmony_3.2.8.3-20221028_193053-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221028_193739/version-Master_Version-OpenHarmony_3.2.8.3-20221028_193739-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221029115408/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/defaultvalue/20221029121504/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-10-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027_120743/version-Master_Version-OpenHarmony_3.2.8.3-20221027_120743-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027_080736/version-Master_Version-OpenHarmony_3.2.8.3-20221027_080736-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027_080623/version-Master_Version-OpenHarmony_3.2.8.3-20221027_080623-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-10-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027_080633/version-Master_Version-OpenHarmony_3.2.8.3-20221027_080633-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027_150741/version-Master_Version-OpenHarmony_3.2.8.3-20221027_150741-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027_080630/version-Master_Version-OpenHarmony_3.2.8.3-20221027_080630-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027024128/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221027013747/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.2(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.2(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.2(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-10-22**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022_120739/version-Master_Version-OpenHarmony_3.2.8.2-20221022_120739-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022_120630/version-Master_Version-OpenHarmony_3.2.8.2-20221022_120630-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022_120740/version-Master_Version-OpenHarmony_3.2.8.2-20221022_120740-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-10-22**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022_120635/version-Master_Version-OpenHarmony_3.2.8.2-20221022_120635-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022_120751/version-Master_Version-OpenHarmony_3.2.8.2-20221022_120751-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022_120751/version-Master_Version-OpenHarmony_3.2.8.2-20221022_120751-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022134407/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221022124658/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-10-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020_183218/version-Master_Version-OpenHarmony_3.2.8.2-20221020_183218-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020_183233/version-Master_Version-OpenHarmony_3.2.8.2-20221020_183233-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020_183139/version-Master_Version-OpenHarmony_3.2.8.2-20221020_183139-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-10-20**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020_183240/version-Master_Version-OpenHarmony_3.2.8.2-20221020_183240-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020_180751/version-Master_Version-OpenHarmony_3.2.8.2-20221020_180751-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020_180758/version-Master_Version-OpenHarmony_3.2.8.2-20221020_180758-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020191420/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.2/20221020191435/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.8.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.8.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.8.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-10-14**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160939/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160939-hispark_pegasus.tar.gz |
| UniProton-stm32f407zg版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160527/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160527-UniProton-stm32f407zg.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160831/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160831-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160940/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160940-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-10-14**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160605/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160605-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160952/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160952-ohos-sdk-full.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014_160940/version-Master_Version-OpenHarmony_3.2.8.1-20221014_160940-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014163806/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.1/20221014170245/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.5(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.5(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.5(sp5)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930_121804/version-Master_Version-OpenHarmony_3.2.7.5-20220930_121804-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930_081224/version-Master_Version-OpenHarmony_3.2.7.5-20220930_081224-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930_081336/version-Master_Version-OpenHarmony_3.2.7.5-20220930_081336-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-30**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930_081226/version-Master_Version-OpenHarmony_3.2.7.5-20220930_081226-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930_101238/version-Master_Version-OpenHarmony_3.2.7.5-20220930_101238-ohos-sdk.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220930_101233/version-Daily_Version-OpenHarmony_3.2.7.5-20220930_101233-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930003510/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220930023227/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.5(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.5(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.5(sp3)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927_141231/version-Master_Version-OpenHarmony_3.2.7.5-20220927_141231-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927_081340/version-Master_Version-OpenHarmony_3.2.7.5-20220927_081340-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927_081225/version-Master_Version-OpenHarmony_3.2.7.5-20220927_081225-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927_081226/version-Master_Version-OpenHarmony_3.2.7.5-20220927_081226-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927_081234/version-Master_Version-OpenHarmony_3.2.7.5-20220927_081234-ohos-sdk.tar.gz |
| ohos-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220927_021228/version-Daily_Version-OpenHarmony_3.2.7.5-20220927_021228-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927003148/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220927023130/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.5(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.5(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.5(sp2)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-24**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924_175659/version-Master_Version-OpenHarmony_3.2.7.5-20220924_175659-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924_161331/version-Master_Version-OpenHarmony_3.2.7.5-20220924_161331-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924_161341/version-Master_Version-OpenHarmony_3.2.7.5-20220924_161341-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-24**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924_161245/version-Master_Version-OpenHarmony_3.2.7.5-20220924_161245-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924_161229/version-Master_Version-OpenHarmony_3.2.7.5-20220924_161229-ohos-sdk.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220924_161229/version-Daily_Version-OpenHarmony_3.2.7.5-20220924_161229-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924172042/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220924023700/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.5(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.5(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.5(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-22**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220922_081223/version-Master_Version-OpenHarmony_3.2.7.5-20220922_081223-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220922_081341/version-Master_Version-OpenHarmony_3.2.7.5-20220922_081341-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220922_081228/version-Master_Version-OpenHarmony_3.2.7.5-20220922_081228-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-22**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220921_201228/version-Master_Version-OpenHarmony_3.2.7.5-20220921_201228-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220922_081228/version-Master_Version-OpenHarmony_3.2.7.5-20220922_081228-ohos-sdk.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220922_081253/version-Daily_Version-OpenHarmony_3.2.7.5-20220922_081253-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220922003155/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220922023240/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220920_081225/version-Master_Version-OpenHarmony_3.2.7.5-20220920_081225-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220920_081233/version-Master_Version-OpenHarmony_3.2.7.5-20220920_081233-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220920_081230/version-Master_Version-OpenHarmony_3.2.7.5-20220920_081230-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-20**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220920_081226/version-Master_Version-OpenHarmony_3.2.7.5-20220920_081226-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.5/20220920_021226/version-Master_Version-OpenHarmony_3.2.7.5-20220920_021226-ohos-sdk.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220920_021226/version-Daily_Version-OpenHarmony_3.2.7.5-20220920_021226-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220920003124/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.5/20220920023225/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-15**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220915_181345/version-Master_Version-OpenHarmony_3.2.7.3-20220915_181345-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220915_181344/version-Master_Version-OpenHarmony_3.2.7.3-20220915_181344-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220915_181341/version-Master_Version-OpenHarmony_3.2.7.3-20220915_181341-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-15**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220915_181230/version-Master_Version-OpenHarmony_3.2.7.3-20220915_181230-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220916_095352/version-Master_Version-OpenHarmony_3.2.7.3-20220916_095352-ohos-sdk.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220916_095407/version-Master_Version-OpenHarmony_3.2.7.3-20220916_095407-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220915180506/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.3/20220915181322/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-8**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908_080953/version-Master_Version-OpenHarmony_3.2.7.2-20220908_080953-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908_080828/version-Master_Version-OpenHarmony_3.2.7.2-20220908_080828-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908_080823/version-Master_Version-OpenHarmony_3.2.7.2-20220908_080823-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-8**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908_080825/version-Master_Version-OpenHarmony_3.2.7.2-20220908_080825-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.2/20220908_080825/version-Daily_Version-OpenHarmony_3.2.7.2-20220908_080825-ohos-sdk-full.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908_080829/version-Master_Version-OpenHarmony_3.2.7.2-20220908_080829-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908111138/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.2/20220908091953/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.7.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.7.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.7.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-9-2**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.1/20220902_160827/version-Master_Version-OpenHarmony_3.2.7.1-20220902_160827-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.1/20220902_160827/version-Master_Version-OpenHarmony_3.2.7.1-20220902_160827-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.1/20220902_160828/version-Master_Version-OpenHarmony_3.2.7.1-20220902_160828-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-9-2**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.1/20220902_160824/version-Master_Version-OpenHarmony_3.2.7.1-20220902_160824-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.7.1/20220902_160825/version-Daily_Version-OpenHarmony_3.2.7.1-20220902_160825-ohos-sdk-full.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.7.1/20220902_160828/version-Master_Version-OpenHarmony_3.2.7.1-20220902_160828-ohos-sdk-public.tar.gz |

## OpenHarmony_3.2.6.5(sp6)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.5(sp6) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.5(sp6)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-31**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831_221732/version-Master_Version-OpenHarmony_3.2.6.5-20220831_221732-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831_221851/version-Master_Version-OpenHarmony_3.2.6.5-20220831_221851-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831_221755/version-Master_Version-OpenHarmony_3.2.6.5-20220831_221755-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-31**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831_221303/version-Master_Version-OpenHarmony_3.2.6.5-20220831_221303-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831_160626/version-Master_Version-OpenHarmony_3.2.6.5-20220831_160626-ohos-sdk-full.tar.gz |
| sdk-public(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831_160712/version-Master_Version-OpenHarmony_3.2.6.5-20220831_160712-ohos-sdk-public.tar.gz |
| mac-sdk: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831121258/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220831153335/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.6.5(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.5(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.5(sp3)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-29**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220829_103924/version-Master_Version-OpenHarmony_3.2.6.5-20220829_103924-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220829_103941/version-Master_Version-OpenHarmony_3.2.6.5-20220829_103941-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220829_103846/version-Master_Version-OpenHarmony_3.2.6.5-20220829_103846-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-29**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220829_103856/version-Master_Version-OpenHarmony_3.2.6.5-20220829_103856-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220829_120627/version-Master_Version-OpenHarmony_3.2.6.5-20220829_120627-ohos-sdk-full.tar.gz |
| mac-sdk(x86): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220829140825/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.6.5(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.5(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.5(sp2)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220827_080627/version-Master_Version-OpenHarmony_3.2.6.5-20220827_080627-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220827_080626/version-Master_Version-OpenHarmony_3.2.6.5-20220827_080626-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220827_080738/version-Master_Version-OpenHarmony_3.2.6.5-20220827_080738-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220827_080627/version-Master_Version-OpenHarmony_3.2.6.5-20220827_080627-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/fan20220827_100626/version-Master_Version-OpenHarmony_3.2.6.5-20220827_100626-ohos-sdk-full.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220827083403/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-sdk(x86): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220827060702/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.6.5(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.5(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.5(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-25**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825_080628/version-Master_Version-OpenHarmony_3.2.6.5-20220825_080628-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825_080625/version-Master_Version-OpenHarmony_3.2.6.5-20220825_080625-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825_080628/version-Master_Version-OpenHarmony_3.2.6.5-20220825_080628-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-25**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825_080627/version-Master_Version-OpenHarmony_3.2.6.5-20220825_080627-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825_080627/version-Master_Version-OpenHarmony_3.2.6.5-20220825_080627-ohos-sdk-full.tar.gz |
| sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825_080625/version-Master_Version-OpenHarmony_3.2.6.5-20220825_080625-ohos-sdk-public.tar.gz |
| mac-sdk(x86): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220825060014/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.6.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-23**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823_180627/version-Master_Version-OpenHarmony_3.2.6.5-20220823_180627-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823_180631/version-Master_Version-OpenHarmony_3.2.6.5-20220823_180631-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823_180642/version-Master_Version-OpenHarmony_3.2.6.5-20220823_180642-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-23**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823_180627/version-Master_Version-OpenHarmony_3.2.6.5-20220823_180627-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823_160743/version-Master_Version-OpenHarmony_3.2.6.5-20220823_160743-ohos-sdk-full.tar.gz |
| sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823_160630/version-Master_Version-OpenHarmony_3.2.6.5-20220823_160630-ohos-sdk-public.tar.gz |
| mac-sdk(x86): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.5/20220823181246/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.6.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-18**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818_080616/version-Master_Version-OpenHarmony_3.2.6.3-20220818_080616-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818_080623/version-Master_Version-OpenHarmony_3.2.6.3-20220818_080623-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818_080737/version-Master_Version-OpenHarmony_3.2.6.3-20220818_080737-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-18**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818_080756/version-Master_Version-OpenHarmony_3.2.6.3-20220818_080756-dayu200.tar.gz |
| sdk(windows+linux): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818_080623/version-Master_Version-OpenHarmony_3.2.6.3-20220818_080623-ohos-sdk-full.tar.gz |
| sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818_080754/version-Master_Version-OpenHarmony_3.2.6.3-20220818_080754-ohos-sdk-public.tar.gz |
| mac-sdk-public: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818084534/L2-SDK-MAC-PUBLIC.tar.gz |
| mac-sdk(x86): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.3/20220818061628/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.6.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-11**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.2/20220811_080539/version-Master_Version-OpenHarmony_3.2.6.2-20220811_080539-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.2/20220811_080429/version-Master_Version-OpenHarmony_3.2.6.2-20220811_080429-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.2/20220811_080537/version-Master_Version-OpenHarmony_3.2.6.2-20220811_080537-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-11**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.2/20220811_080425/version-Master_Version-OpenHarmony_3.2.6.2-20220811_080425-dayu200.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.6.2/20220811_080425/version-Daily_Version-OpenHarmony_3.2.6.2-20220811_080425-ohos-sdk-full.tar.gz |
| mac-sdk(x86):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.2/20220811063933/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.6.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.6.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.6.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-8-4**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.1/20220804_080426/version-Master_Version-OpenHarmony_3.2.6.1-20220804_080426-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.1/20220804_080423/version-Master_Version-OpenHarmony_3.2.6.1-20220804_080423-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.1/20220804_080426/version-Master_Version-OpenHarmony_3.2.6.1-20220804_080426-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-8-4**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_32位版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.1/20220804_080425/version-Master_Version-OpenHarmony_3.2.6.1-20220804_080425-dayu200.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.6.1/20220804_080424/version-Daily_Version-OpenHarmony_3.2.6.1-20220804_080424-ohos-sdk-full.tar.gz |
| mac-sdk(x86):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.1/20220804100000/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.6.1/20220804102712/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.5.5(sp6)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.5(sp6) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.5(sp6)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220730_080840/version-Master_Version-OpenHarmony_3.2.5.5-20220730_080840-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220730_080721/version-Master_Version-OpenHarmony_3.2.5.5-20220730_080721-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220730_080853/version-Master_Version-OpenHarmony_3.2.5.5-20220730_080853-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-30**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220730_080724/version-Master_Version-OpenHarmony_3.2.5.5-20220730_080724-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220730_080721/version-Master_Version-OpenHarmony_3.2.5.5-20220730_080721-ohos-sdk-full.tar.gz  |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220730064604/L2-SDK-MAC-FULL.tar.gz  |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220730100000/L2-SDK-MAC-M1-FULL.tar.gz |
| sdk_public(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220730_140000/version-Master_Version-OpenHarmony_3.2.5.5-20220730_140000-ohos-sdk-public.tar.gz |
| sdk_public(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220730130000/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.2.5.5(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.5(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.5(sp5)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-29**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220728_180735/version-Master_Version-OpenHarmony_3.2.5.5-20220728_180735-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220728_180844/version-Master_Version-OpenHarmony_3.2.5.5-20220728_180844-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220728_180735/version-Master_Version-OpenHarmony_3.2.5.5-20220728_180735-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-29**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220728_232745/version-Master_Version-OpenHarmony_3.2.5.5-20220728_232745-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220728_180730/version-Master_Version-OpenHarmony_3.2.5.5-20220728_180730-ohos-sdk-full.tar.gz  |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220728204012/L2-SDK-MAC-FULL.tar.gz  |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220728200644/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.5.5(sp3)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.5(sp3) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.5(sp3)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220727_124636/version-Master_Version-OpenHarmony_3.2.5.5-20220727_124636-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220727_124702/version-Master_Version-OpenHarmony_3.2.5.5-20220727_124702-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220727_124555/version-Master_Version-OpenHarmony_3.2.5.5-20220727_124555-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220727_124618/version-Master_Version-OpenHarmony_3.2.5.5-20220727_124618-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220727_124607/version-Master_Version-OpenHarmony_3.2.5.5-20220727_124607-ohos-sdk-full.tar.gz  |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220727140705/L2-SDK-MAC-FULL.tar.gz  |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220727132749/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.5.5(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.5(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.5(sp2)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-26**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220725_170341/version-Master_Version-OpenHarmony_3.2.5.5-20220725_170341-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220725_170246/version-Master_Version-OpenHarmony_3.2.5.5-20220725_170246-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220725_170404/version-Master_Version-OpenHarmony_3.2.5.5-20220725_170404-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-26**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220725_170333/version-Master_Version-OpenHarmony_3.2.5.5-20220725_170333-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220725_170257/version-Master_Version-OpenHarmony_3.2.5.5-20220725_170257-ohos-sdk-full.tar.gz  |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220725184630/L2-SDK-MAC-FULL.tar.gz  |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220725180532/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.5.5(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.5(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.5(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-23**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220723_140733/version-Master_Version-OpenHarmony_3.2.5.5-20220723_140733-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220723_140843/version-Master_Version-OpenHarmony_3.2.5.5-20220723_140843-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220723_140842/version-Master_Version-OpenHarmony_3.2.5.5-20220723_140842-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-23**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220723_140733/version-Master_Version-OpenHarmony_3.2.5.5-20220723_140733-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220723_080733/version-Master_Version-OpenHarmony_3.2.5.5-20220723_080733-ohos-sdk-full.tar.gz  |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220723062343/L2-SDK-MAC-FULL.tar.gz  |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220723162240/L2-SDK-MAC-M1-FULL.tar.gz |

## OpenHarmony_3.2.5.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-21**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220721_080841/version-Master_Version-OpenHarmony_3.2.5.5-20220721_080841-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220721_020740/version-Master_Version-OpenHarmony_3.2.5.5-20220721_020740-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220721_020730/version-Master_Version-OpenHarmony_3.2.5.5-20220721_020730-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-21**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220721_020733/version-Master_Version-OpenHarmony_3.2.5.5-20220721_020733-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.5/20220721_020731/version-Master_Version-OpenHarmony_3.2.5.5-20220721_020731-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220721062405/L2-SDK-MAC-FULL.tar.gz |
| mac-sdk-M1:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta2/20220721180000/L2-SDK-MAC-M1-FULL.tar.gz |


## OpenHarmony_3.2.5.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-14**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.3/20220714_020840/version-Master_Version-OpenHarmony_3.2.5.3-20220714_020840-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.3/20220714_020838/version-Master_Version-OpenHarmony_3.2.5.3-20220714_020838-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.3/20220714_020727/version-Master_Version-OpenHarmony_3.2.5.3-20220714_020727-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-14**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.3/20220714_020726/version-Master_Version-OpenHarmony_3.2.5.3-20220714_020726-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.3/20220714_020728/version-Master_Version-OpenHarmony_3.2.5.3-20220714_020728-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.3/20220714070217/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.5.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.2版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-7**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.2/20220707_140156/version-Master_Version-OpenHarmony_3.2.5.2-20220707_140156-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.2/20220707_140040/version-Master_Version-OpenHarmony_3.2.5.2-20220707_140040-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.2/20220707_140154/version-Master_Version-OpenHarmony_3.2.5.2-20220707_140154-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-7**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.2/20220707_174126/version-Master_Version-OpenHarmony_3.2.5.2-20220707_174126-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.2/20220707_140031/version-Master_Version-OpenHarmony_3.2.5.2-20220707_140031-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.2/20220707165858/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.5.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.5.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.5.1版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.1/20220630_100954/version-Master_Version-OpenHarmony_3.2.5.1-20220630_100954-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.1/20220630_040030/version-Master_Version-OpenHarmony_3.2.5.1-20220630_040030-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.1/20220630_040031/version-Master_Version-OpenHarmony_3.2.5.1-20220630_040031-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-30**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.1/20220630_020140/version-Master_Version-OpenHarmony_3.2.5.1-20220630_020140-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.1/20220630_040146/version-Master_Version-OpenHarmony_3.2.5.1-20220630_040146-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.5.1/20220630054544/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.3.5(sp5)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.5(sp5) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.5(sp5)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-7-1**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220701_103525/version-Master_Version-OpenHarmony_3.2.3.5-20220701_103525-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220701_103741/version-Master_Version-OpenHarmony_3.2.3.5-20220701_103741-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220701_103742/version-Master_Version-OpenHarmony_3.2.3.5-20220701_103742-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-7-1**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220701_153006/version-Master_Version-OpenHarmony_3.2.3.5-20220701_153006-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220701_103823/version-Master_Version-OpenHarmony_3.2.3.5-20220701_103823-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220701113722/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.3.5(sp2)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.5(sp2) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.5(sp2)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-28**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220628_140025/version-Master_Version-OpenHarmony_3.2.3.5-20220628_140025-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220628_140139/version-Master_Version-OpenHarmony_3.2.3.5-20220628_140139-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220628_140031/version-Master_Version-OpenHarmony_3.2.3.5-20220628_140031-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-28**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220628_140026/version-Master_Version-OpenHarmony_3.2.3.5-20220628_140026-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220628_140029/version-Master_Version-OpenHarmony_3.2.3.5-20220628_140029-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220628153129/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.3.5(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.5(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.5(sp1)版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-25**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220625_183957/version-Master_Version-OpenHarmony_3.2.3.5-20220625_183957-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220625_180020/version-Master_Version-OpenHarmony_3.2.3.5-20220625_180020-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220625_180157/version-Master_Version-OpenHarmony_3.2.3.5-20220625_180157-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-25**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220624_140030/version-Master_Version-OpenHarmony_3.2.3.5-20220624_140030-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220625_140029/version-Master_Version-OpenHarmony_3.2.3.5-20220625_140029-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220625080913/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.3.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.5版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-23**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220623_040026/version-Master_Version-OpenHarmony_3.2.3.5-20220623_040026-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220623_040035/version-Master_Version-OpenHarmony_3.2.3.5-20220623_040035-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220623_040141/version-Master_Version-OpenHarmony_3.2.3.5-20220623_040141-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-23**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220623_020031/version-Master_Version-OpenHarmony_3.2.3.5-20220623_020031-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220623_040028/version-Master_Version-OpenHarmony_3.2.3.5-20220623_040028-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.5/20220623083031/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.3.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.3版本:  |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.3/20220615_140141/version-Master_Version-OpenHarmony_3.2.3.3-20220615_140141-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.3/20220616_040027/version-Master_Version-OpenHarmony_3.2.3.3-20220616_040027-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.3/20220616_040142/version-Master_Version-OpenHarmony_3.2.3.3-20220616_040142-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-16**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.3/20220616_020034/version-Master_Version-OpenHarmony_3.2.3.3-20220616_020034-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.3/20220616_040155/version-Master_Version-OpenHarmony_3.2.3.3-20220616_040155-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.3/20220616210123/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.3.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.2版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-9**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.2/20220609_040154/version-Master_Version-OpenHarmony_3.2.3.2-20220609_040154-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.2/20220609_040146/version-Master_Version-OpenHarmony_3.2.3.2-20220609_040146-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.2/20220609_040141/version-Master_Version-OpenHarmony_3.2.3.2-20220609_040141-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-9**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony_3.2.3.2/20220609_171912/version-Daily_Version-OpenHarmony_3.2.3.2-20220609_171912-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.2/20220609_040034/version-Master_Version-OpenHarmony_3.2.3.2-20220609_040034-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.2/20220609082639/L2-SDK-MAC-FULL.tar.gz |

## OpenHarmony_3.2.3.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.3.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.3.1版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-6-2**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.1/20220602_040141/version-Master_Version-OpenHarmony_3.2.3.1-20220602_040141-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.1/20220602_040141/version-Master_Version-OpenHarmony_3.2.3.1-20220602_040141-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.1/20220602_040143/version-Master_Version-OpenHarmony_3.2.3.1-20220602_040143-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-6-2**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.1/20220602_020147/version-Master_Version-OpenHarmony_3.2.3.1-20220602_020147-dayu200-arm64.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.1/20220602_040149/version-Master_Version-OpenHarmony_3.2.3.1-20220602_040149-ohos-sdk-full.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.3.1/20220602084121/L2-SDK-MAC-FULL.tar.gz |


## OpenHarmony_3.2.2.5(Beta1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.2.5(Beta1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.2.5(Beta1)版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-5-31**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHamrony_3.2_Beta1/20220531_180401/version-Master_Version-OpenHamrony_3.2_Beta1-20220531_180401-hispark_pegasus.tar.gz|
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHamrony_3.2_Beta1/20220531_180516/version-Master_Version-OpenHamrony_3.2_Beta1-20220531_180516-hispark_taurus_LiteOS.tar.gz|
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHamrony_3.2_Beta1/20220531_180445/version-Master_Version-OpenHamrony_3.2_Beta1-20220531_180445-hispark_taurus_Linux.tar.gz|
| **L2****转测试时间：2022-5-31**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.2_Beta1/20220531_181657/version-Release_Version-OpenHarmony_3.2_Beta1-20220531_181657-dayu200-arm64.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.2_Beta1/20220531_181808/version-Release_Version-OpenHarmony_3.2_Beta1-20220531_181808-ohos-sdk-full.tar.gz|
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta1/20220531200127/L2-SDK-MAC-full.tar.gz|

## OpenHarmony_3.2.2.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.2.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.2.5版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-5-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony-3.2-Beta1/20220526_000155/version-Master_Version-OpenHarmony-3.2-Beta1-20220526_000155-hispark_pegasus.tar.gz|
| hispark_taurus版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony-3.2-Beta1/20220526_120141/version-Master_Version-OpenHarmony-3.2-Beta1-20220526_120141-hispark_taurus_LiteOS.tar.gz|
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony-3.2-Beta1/20220526_120141/version-Master_Version-OpenHarmony-3.2-Beta1-20220526_120141-hispark_taurus_Linux.tar.gz|
| **L2****转测试时间：2022-5-27**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony-3.2-Beta1/20220526_140029/version-Master_Version-OpenHarmony-3.2-Beta1-20220526_140029-dayu200-arm64.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony-3.2-Beta1/20220526_020147/version-Master_Version-OpenHarmony-3.2-Beta1-20220526_020147-ohos-sdk.tar.gz|
| sdk(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta1/20220526052328/L2-SDK-MAC.tar.gz|

## OpenHarmony_3.2.2.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.2.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.2.3版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-5-24**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony-3.2-Beta1/20220524_000156/version-Daily_Version-OpenHarmony-3.2-Beta1-20220524_000156-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony-3.2-Beta1/20220524_000156/version-Daily_Version-OpenHarmony-3.2-Beta1-20220524_000156-hispark_taurus_LiteOS.tar.gz|
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony-3.2-Beta1/20220524_000143/version-Daily_Version-OpenHarmony-3.2-Beta1-20220524_000143-hispark_taurus_Linux.tar.gz|
| **L2****转测试时间：2022-5-24**                             |
| **L2****转测试版本获取路径：**                               |
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony-3.2-Beta1/20220524_020049/version-Daily_Version-OpenHarmony-3.2-Beta1-20220524_020049-dayu200-arm64.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Daily_Version/OpenHarmony-3.2-Beta1/20220524_020153/version-Daily_Version-OpenHarmony-3.2-Beta1-20220524_020153-ohos-sdk.tar.gz|
| sdk(mac): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2_Beta1/20220524052538/L2-SDK-MAC.tar.gz|

## OpenHarmony_3.2.2.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.2.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.2.2版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-5-13**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.2/20220513_103611/version-Master_Version-OpenHarmony_3.2.2.2-20220513_103611-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.2/20220513_040137/version-Master_Version-OpenHarmony_3.2.2.2-20220513_040137-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.2/20220513_040141/version-Master_Version-OpenHarmony_3.2.2.2-20220513_040141-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-5-13**                             |
| **L2****转测试版本获取路径：**                               |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.2/20220515_180143/version-Master_Version-OpenHarmony_3.2.2.2-20220515_180143-dayu200.tar.gz|
| rk3568_arm64版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.2/20220515_190712/version-Master_Version-OpenHarmony_3.2.2.2-20220515_190712-dayu200-arm64.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.2/20220513_040031/version-Master_Version-OpenHarmony_3.2.2.2-20220513_040031-ohos-sdk.tar.gz|
| sdk(mac): https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.2.2.2/20220513_040307/L2-SDK-MAC.tar.gz|

## OpenHarmony_3.2.2.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.2.2.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.2.2.1版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-5-6**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.1/20220428_180022/version-Master_Version-OpenHarmony_3.2.2.1-20220428_180022-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.1/20220428_180130/version-Master_Version-OpenHarmony_3.2.2.1-20220428_180130-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.1/20220428_180015/version-Master_Version-OpenHarmony_3.2.2.1-20220428_180015-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-5-6**                             |
| **L2****转测试版本获取路径：**                               |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.1/20220428_180140/version-Master_Version-OpenHarmony_3.2.2.1-20220428_180140-dayu200.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.2.1/20220428_040157/version-Master_Version-OpenHarmony_3.2.2.1-20220428_040157-ohos-sdk.tar.gz|
| sdk(mac): https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.2.2.1/20220428_081346/L2-SDK-MAC.tar.gz |


## OpenHarmony_4.0.1.3/3.2.1.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.1.3/3.2.1.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.1.3/3.2.1.3版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-4-22**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.3/20220421_180132/version-Master_Version-openharmony_4.0.1.3-20220421_180132-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.3/20220421_180033/version-Master_Version-openharmony_4.0.1.3-20220421_180033-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.3/20220421_180141/version-Master_Version-openharmony_4.0.1.3-20220421_180141-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-4-22**                             |
| **L2****转测试版本获取路径：**                               |
| L2-hi3516版本: **|
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.3/20220421_180133/version-Master_Version-openharmony_4.0.1.3-20220421_180133-dayu200.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.3/20220421_040138/version-Master_Version-openharmony_4.0.1.3-20220421_040138-ohos-sdk.tar.gz|
| sdk(mac): https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_4.0.1.3/20220422_115951/L2-SDK-MAC.tar.gz |


## OpenHarmony_4.0.1.2版本转测试信息：

| ********转测试版本号：    OpenHarmony_4.0.1.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.1.2版本:   |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-4-15**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.2/20220415_101510/version-Master_Version-openharmony_4.0.1.2-20220415_101510-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.2/20220415_040138/version-Master_Version-openharmony_4.0.1.2-20220415_040138-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.2/20220415_040134/version-Master_Version-openharmony_4.0.1.2-20220415_040134-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：**                             |
| **L2****转测试版本获取路径：**                               |
| L2-hi3516版本: **|
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.2/20220415_040144/version-Master_Version-openharmony_4.0.1.2-20220415_040144-dayu200.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.2/20220415_040139/version-Master_Version-openharmony_4.0.1.2-20220415_040139-ohos-sdk.tar.gz|
| sdk(mac): https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_4.0.1.2/20220415_080054/L2-SDK-MAC.tar.gz |
 

## OpenHarmony 4.0.1.2版本特性清单：

| no   | issue                                                        | feture description                                     | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
| 1    | [I50DOL]| Stage模型支持Worker机制 | 元能力子系统 | SIG_ApplicationFramework | [@zhongjianfei](https://gitee.com/zhongjianfei) |
| 2   | [I51UGK] | 【syscap_codec】提供syscap编解码native接口支持写入DP | 研发工具链子系统 | SIG_ApplicationFramework | [@wuyunjie] |
| 3   | [I52G5Q] | 【MR】【DMS】添加群组校验机制 | 系统服务管理 | SIG_DataManagement | [@jiacan] (https://gitee.com/cangegegege)|
| 4   | [I50DAQ] | 卡片使用记录上报 | 元能力子系统 | SIG_ApplicationFramework | [@zhongjianfei](https://gitee.com/zhongjianfei) |
| 5    | [I524TT] | 支持对测试框架的配置 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao]（https://gitee.com/shuaytao）|
| 6   | [I4UD9W]| 支持内核态动态驱动加载机制，提高设备驱动开发、调试效率 | 驱动子系统 | SIG_DriverFramework | [@yuanbo](https://gitee.com/yuanbogit) |
| 7    | [I524WG] | 支持系统应用不允许清除的能力 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao]（https://gitee.com/shuaytao） |
| 8   | [I526UP] | 持@system.battery 电池信息查询接口 | 电源服务 | SIG_DistributedHardwareManagement | [@hujun211](https://gitee.com/hujun211) |
| 9    | [I524WP] | 查询指定应用是否安装 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao]（https://gitee.com/shuaytao） |


## OpenHarmony_4.0.1.1版本转测试信息：
| ********转测试版本号：    OpenHarmony_4.0.1.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_4.0.1.1版本:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-4-8**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.1/20220408_092241/version-Master_Version-openharmony_4.0.1.1-20220408_092241-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.1/20220408_140154/version-Master_Version-openharmony_4.0.1.1-20220408_140154-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.1/20220408_140042/version-Master_Version-openharmony_4.0.1.1-20220408_140042-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-4-8**                             |
| **L2****转测试版本获取路径：**                               |
| L2-hi3516版本: http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.1/20220408_140156/version-Master_Version-openharmony_4.0.1.1-20220408_140156-hispark_taurus_L2.tar.gz|
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.1/20220408_100037/version-Master_Version-openharmony_4.0.1.1-20220408_100037-dayu200.tar.gz|
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Master_Version/openharmony_4.0.1.1/20220408_100157/version-Master_Version-openharmony_4.0.1.1-20220408_100157-ohos-sdk.tar.gz|
| sdk(mac): https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_4.0.1.1/20220408_080007/L2-SDK-MAC.tar.gz |

## OpenHarmony 4.0.1.1版本特性清单：

| no   | issue                                                        | feture description                                     | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
| 1    | [I50DBH](https://gitee.com/openharmony/aafwk_standard/issues/I50DBH) | 【新增特性】卡片数据支持携带图片 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |
| 2   | [I50DBT](https://gitee.com/openharmony/aafwk_standard/issues/I50DBT) | 【新增特性】FA卡片能力补齐-formManager重构 | 标准系统 | SIG_ApplicationFramework | [@autumn330](https://gitee.com/autumn330) |

## OpenHarmony_3.1_Release版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1_Release *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1_Release版本:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1_Release/20220330_163001/version-Release_Version-OpenHarmony_3.1_Release-20220330_163001-hispark_pegasus.tar.gz|
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1_Release/20220330_161803/version-Release_Version-OpenHarmony_3.1_Release-20220330_161803-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1_Release/20220330_162654/version-Release_Version-OpenHarmony_3.1_Release-20220330_162654-hispark_taurus_Linux_img.tar.gz |
| **L2****转测试时间：2022-3-30**                             |
| **L2****转测试版本获取路径：**                               |
| L2-hi3516版本: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1_Release/20220330_162742/version-Release_Version-OpenHarmony_3.1_Release-20220330_162742-hispark_taurus_L2.tar.gz|
| RK3568版本: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1_Release/20220330_162631/version-Release_Version-OpenHarmony_3.1_Release-20220330_162631-dayu200.tar.gz|
| sdk(windows+linux)API8:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1_Release/20220330_162909/version-Release_Version-OpenHarmony_3.1_Release-20220330_162909-ohos-sdk.tar.gz|
| sdk(mac)API8: https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_3.1_Release/20220330_172526/L2-SDK-MAC.tar.gz |
| sdk(windows+linux)API9:http://download.ci.openharmony.cn/version/Daily_Version/ohos-sdk/20220330_180024/version-Daily_Version-ohos-sdk-20220330_180024-ohos-sdk.tar.gz|
| sdk(mac)API9:  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1_API9_SDK/20220330_185837/L2-SDK-MAC.tar.gz |

## OpenHarmony_3.1.5.5（sp7）版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1.5.5（sp7） *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第6轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-26**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp7/20220326_020134/version-Master_Version-OpenHarmony_3.1.5.5_sp7-20220326_020134-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp7/20220326_020022/version-Master_Version-OpenHarmony_3.1.5.5_sp7-20220326_020022-hispark_taurus_LiteOS_img.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp7/20220326_020127/version-Master_Version-OpenHarmony_3.1.5.5_sp7-20220326_020127-hispark_taurus_Linux_img.tar.gz |
| **L2****转测试时间：2022-3-26**                             |
| **L2****转测试版本获取路径：**                               |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp7/20220325_220024/version-Master_Version-OpenHarmony_3.1.5.5_sp7-20220325_220024-dayu200.tar.gz|
| sdk(windows+linux)API8:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp7/20220326_020024/version-Master_Version-OpenHarmony_3.1.5.5_sp7-20220326_020024-ohos-sdk.tar.gz|
| sdk(mac)API8:  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.5_sp7/20220326_030010/L2-SDK-MAC.tar.gz |
| sdk(windows+linux)API9:http://download.ci.openharmony.cn/version/Daily_Version/ohos-sdk/20220325_210027/version-Daily_Version-ohos-sdk-20220325_210027-ohos-sdk.tar.gz|
| sdk(mac)API9:  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1_API9_SDK/20220326_080032/L2-SDK-MAC.tar.gz |

## OpenHarmony_3.1.5.5（sp6）版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1.5.5（sp6） *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第6轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-23**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_130125/version-Master_Version-OpenHarmony_3.1.5.5_sp3-20220319_130125-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp6/20220322_130124/version-Master_Version-OpenHarmony_3.1.5.5_sp6-20220322_130124-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/openharmony_3.1.5.5_sp6/20220322_223015/version-Master_Version-openharmony_3.1.5.5_sp6-20220322_223015-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-3-23**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows:http://download.ci.openharmony.cn/version/Master_Version/openharmony_3.1.5.5_sp6/20220322_223056/version-Master_Version-openharmony_3.1.5.5_sp6-20220322_223056-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac:  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.5_sp5/20220322_032224/L2-SDK-MAC.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/openharmony_3.1.5.5_sp6/20220322_223038/version-Master_Version-openharmony_3.1.5.5_sp6-20220322_223038-dayu200.tar.gz |


## OpenHarmony_3.1.5.5（sp3）版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1.5.5（sp3） *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第5轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-19**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_130125/version-Master_Version-OpenHarmony_3.1.5.5_sp3-20220319_130125-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_130116/version-Master_Version-OpenHarmony_3.1.5.5_sp3-20220319_130116-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_130013/version-Master_Version-OpenHarmony_3.1.5.5_sp3-20220319_130013-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-3-19**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_130024/version-Master_Version-OpenHarmony_3.1.5.5_sp3-20220319_130024-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac:  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_032043/L2-SDK-MAC.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp3/20220319_123859/version-Master_Version-OpenHarmony_3.1.5.5_sp3-20220319_123859-dayu200.tar.gz |



## OpenHarmony_3.1.5.5（sp2）版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1.5.5（sp2） *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第5轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-15**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp2/20220314_220320/version-Master_Version-OpenHarmony_3.1.5.5_sp2-20220314_220320-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp2/20220314_220320/version-Master_Version-OpenHarmony_3.1.5.5_sp2-20220314_220320-hispark_pegasus.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp2/20220314_220421/version-Master_Version-OpenHarmony_3.1.5.5_sp2-20220314_220421-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-3-15**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp2/20220314_220637/version-Master_Version-OpenHarmony_3.1.5.5_sp2-20220314_220637-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac：  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.5/20220314_195009/L2-SDK-MAC.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5_sp2/20220314_220457/version-Master_Version-OpenHarmony_3.1.5.5_sp2-20220314_220457-dayu200.tar.gz |



## OpenHarmony_3.1.5.5（sp1）版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1.5.5（sp1） *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第5轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-11**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5(sp1)/20220311_120943/version-Master_Version-OpenHarmony_3.1.5.5(sp1)-20220311_120943-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5(sp1)/20220311_121105/version-Master_Version-OpenHarmony_3.1.5.5(sp1)-20220311_121105-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5(sp1)/20220311_120837/version-Master_Version-OpenHarmony_3.1.5.5(sp1)-20220311_120837-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-3-11**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5(sp1)/20220311_121029/version-Master_Version-OpenHarmony_3.1.5.5(sp1)-20220311_121029-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac：  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.5/20220311_053043/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5(sp1)/20220311_121028/version-Master_Version-OpenHarmony_3.1.5.5(sp1)-20220311_121028-hispark_taurus_L2.tar.gz|
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5(sp1)/20220311_120800/version-Master_Version-OpenHarmony_3.1.5.5(sp1)-20220311_120800-dayu200.tar.gz |




## OpenHarmony 3.1.5.5版本转测试信息：
| ********转测试版本号：    OpenHarmony 3.1.5.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第4轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-9**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5/20220309_173020/version-Master_Version-OpenHarmony_3.1.5.5-20220309_173020-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5/20220309_173021/version-Master_Version-OpenHarmony_3.1.5.5-20220309_173021-hispark_taurus_LiteOS_img.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5/20220309_173024/version-Master_Version-OpenHarmony_3.1.5.5-20220309_173024-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-3-9**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5/20220309_173014/version-Master_Version-OpenHarmony_3.1.5.5-20220309_173014-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac：  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.5/20220309_190934/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5/20220309_173039/version-Master_Version-OpenHarmony_3.1.5.5-20220309_173039-hispark_taurus_L2.tar.gz|
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.5/20220309_173127/version-Master_Version-OpenHarmony_3.1.5.5-20220309_173127-dayu200.tar.gz |



## OpenHarmony 3.1.5.3版本转测试信息：
| ********转测试版本号：    OpenHarmony 3.1.5.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第3轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-3-4**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.3/20220303_040123/version-Master_Version-OpenHarmony_3.1.5.3-20220303_040123-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.3/20220303_040131/version-Master_Version-OpenHarmony_3.1.5.3-20220303_040131-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.3/20220303_040129/version-Master_Version-OpenHarmony_3.1.5.3-20220303_040129-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-3-4**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.3/20220303_040134/version-Master_Version-OpenHarmony_3.1.5.3-20220303_040134-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac：  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.3/20220303_134216/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.3/20220304_040132/version-Master_Version-OpenHarmony_3.1.5.3-20220304_040132-hispark_taurus_L2.tar.gz|
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.3/20220304_152415/version-Master_Version-OpenHarmony_3.1.5.3-20220304_152415-dayu200.tar.gz |

## OpenHarmony 3.1.5.3版本特性清单：

| no   | issue                                                        | feture description                                     | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
| 1    | [I4P7F7](https://gitee.com/openharmony/third_party_musl/issues/I4P7F7) | [语言编译运行时子系统]提供Windows，linux，Mac多平台NDK | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 2  | [I4Q9F2](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9F2)  | [内核子系统]【新增特性】支持native动态加载机制   | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 3  | [I4Q9OQ](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9OQ)  | [内核子系统]【新增特性】支持Cortex-M55  | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 4  | [I4Q9PB](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9PB)  | [内核子系统]【增强特性】支持内核性能增强 | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 5  | [I4Q9PZ](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9PZ)  | [内核子系统]【增强特性】支持内核维测增强  | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 6  | [I4Q9QC](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9QC)  | [内核子系统]【增强特性】支持newlib c库与musl c库可平滑切换  | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 7    | [I4QEL7](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL7) | 【新增特性】EDP、HDMI屏幕支持；支持显示设备热插拔事件上报；支持查询显示设备相关信息；支持修改显示分辨率                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 8    | [I4QEL8](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL8) | 【新增特性】Audio支持录音缓存阈值上报、设备加载成功后事件上报                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 9    | [I4U76A](https://gitee.com/openharmony/drivers_peripheral/issues/I4U76A) | 【新增特性】基于HDF驱动框架提供计步器Sensor驱动能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 12    | [I4QT45](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QT45) | 【新增特性】查询指定时间范围内的应用使用历史统计数据 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 13    | [I4QT46](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QT46) | 【新增特性】查询指定时间范围内的应用事件集合 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 14    | [I4QU0G](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0G) | 【新增特性】查询应用程序当前是否不常用应用 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 15    | [I4QU0I](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0I) | 【新增特性】采集数据处理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 16    | [I4QU0K](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0K) | 【新增特性】配置信息变更更新 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 17    | [I4QU0M](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I4QU0M) |  资料专项 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 18    | [I4UTC6](https://gitee.com/openharmony/startup_init_lite/issues/I4UTC6) | 【新增规格】系统进程运行环境沙盒构建   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 19    | [I4UTCO](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCO) | 【增强特性】支持app进程孵化能力增强   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 20    | [I4UTCY](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCY) | 【增强特性】appspawn支持孵化的应用进程回收   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 21    | [I4UTD0](https://gitee.com/openharmony/startup_init_lite/issues/I4UTD0) | 【资料】init部件南向文档需求   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 22    | [I4U27N](https://gitee.com/openharmony/global_resmgr_standard/issues/I4U27N) | 【新增特性】提供获取RawFileDescriptor的js接口                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 23   | [I4VMGZ](https://gitee.com/openharmony/frame_aware_sched/issues/I4VMGZ)  | [内核子系统]【新增特性】支持基础FPS智能感知调度功能     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 24   | [I4U089](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4U089)  | [内核子系统]【新增特性】内核调度支持绘帧线程优先供给机制     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 25 | [I4V226](https://gitee.com/openharmony/docs/issues/I4V226) | 【资料】HUKS密钥管理开发指南和API接口说明 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 26   | [I4W1E4](https://gitee.com/openharmony/security_access_token/issues/I4W1E4)  | 【性能】AccessToken性能需求    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 27   | [I4W1E0](https://gitee.com/openharmony/security_access_token/issues/I4W1E0)  | 【资料】AccessToken资料需求    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 28 | [I4W7ZO](https://gitee.com/openharmony/ark_js_runtime/issues/I4W7ZO?from=project-issue) | 【新增特性】Runtime性能调优 | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |
| 29 | [I4W7ZR](https://gitee.com/openharmony/ark_js_runtime/issues/I4W7ZR?from=project-issue) | 【新增规格】内存管理分配回收功能/HPP GC性能调优/* | 标准系统 | SIG_CompileRuntime | [@huanghuijin](https://gitee.com/huanghuijin) |

## OpenHarmony 3.1.5.2版本转测试信息：
| ********转测试版本号：    OpenHarmony 3.1.5.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代五第2轮测试，验收:    |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-2-27**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.2/20220226_200601/version-Master_Version-OpenHarmony_3.1.5.2-20220226_200601-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.2/20220226_200600/version-Master_Version-OpenHarmony_3.1.5.2-20220226_200600-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.2/20220226_200428/version-Master_Version-OpenHarmony_3.1.5.2-20220226_200428-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-2-27**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.2/20220226_200639/version-Master_Version-OpenHarmony_3.1.5.2-20220226_200639-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac：  https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.2/20220226_184302/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.2/20220226_120131/version-Master_Version-OpenHarmony_3.1.5.2-20220226_120131-hispark_taurus_L2.tar.gz|
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.2/20220227_180019/version-Master_Version-OpenHarmony_3.1.5.2-20220227_180019-dayu200.tar.gz |

## OpenHarmony 3.1.5.2版本特性清单：
| no   | issue                                                        | feture description                           | platform     | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :------------------------------------------- | :----------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4OGLE](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4OGLE) | 【资料】batterystatistic部件资料需求         | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I40IRO](https://gitee.com/openharmony/powermgr_power_manager/issues/I40IRO) | 【系统电源管理服务】 支持休眠和混合睡眠模式  | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4OGD4](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD4?from=project-issue) | 【资料】跨设备任务迁移新增/增强特性资料说明  | 标准系统     | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 4    | [I4OGCZ](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCZ?from=project-issue) | 【资料】跨设备组件oncall调用新增特性资料说明 | 标准系统     | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 5    |[I4PCLE](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLE)|【新增特性】应用环境创建和管理优化|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 6    |[I4PCLJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLJ)|【新增特性】应用启动功耗优化|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 7    |[I4PCLU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLU)|【新增特性】对外接口性能验收|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 8    |[I4PCMC](https://gitee.com/openharmony/aafwk_standard/issues/I4PCMC)|【新增特性】应用启动性能优化|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 9    | [I4PKY4](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY4) | 【showcase特性】DBMS启动与退出               | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 10   | [I4PKY5](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY5) | 【showcase特性】DBMS异常退出的恢复           | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 11   | [I4PKY6](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY6) | 【showcase特性】跨设备获取icon和label        | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 12   | [I4PKY9](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY9) | 【资料】包命令行工具资料说明                 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 13   | [I4PKYC](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYC) | 【资料】config.json配置文件字段说明          | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 14   | [I4PKYJ](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYJ) | 【资料】提供包管理服基本功能的资料说明       | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 15  | [I4Q9SU](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9SU)  | [内核子系统]【增强特性】支持北向接口融合   | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 16    | [I4QEL3](https://gitee.com/openharmony/drivers_framework/issues/I4QEL3) | 【资料】驱动子系统资料刷新                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 17    | [I4QEL4](https://gitee.com/openharmony/usb_manager/issues/I4QEL4) | 【资料】USB子系统文档资料                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 20    | [I4QT3X](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT3X) | 【新增特性】支持日历类及延迟提醒功能 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 21    |[I4RTYQ](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYQ?from=project-issue) |【资料】DSLM部件设备安全等级凭据格式开放和交换协议开放需求| 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
| 22    | [I4RGFY](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4RGFY) | 【DataShare】基于ExtensionAbility新框架重构并提供单设备上跨应用数据共享能力                    | 标准系统   | SIG_DataManagement | [@verystone](https://gitee.com/verystone)         |
| 23   | [I4QU0U](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0U) | 【新增特性】延迟任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 24   | [I4T9KH](https://gitee.com/openharmony/developtools_hapsigner/issues/I4T9KH)|【资料】PKI应用签名工具资料需求|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 25    | [I4UTCF](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCF) | 【新增特性】进程分组及并行启动基础框架   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 26    | [I4UTCM](https://gitee.com/openharmony/startup_init_lite/issues/I4UTCM) | 【新增规格】【新增特性】进程分组启动支持整机不同的启动功能   | 标准系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 27 | [I4UUUU](https://gitee.com/openharmony/security_huks/issues/I4UUUU) | 【新增规格】DEMO测试，提供密钥管理全能力集的测试DEMO | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 28 | [I4TYFR](https://gitee.com/openharmony/security_huks/issues/I4TYFR) | 【新增规格】HUKS在删除应用的情况下，HUKS需要删除相关的密钥数据 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 29 | [I4TYFI](https://gitee.com/openharmony/security_huks/issues/I4TYFI) | 【新增规格】HUKS在删除子用户情况下，需要删除相关的密钥数据 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 30 |[I4TYFA](https://gitee.com/openharmony/security_huks/issues/I4TYFA) | 【新增规格】HUKS支持密钥应用基于APP UID的访问隔离 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 31 | [I4TYF1](https://gitee.com/openharmony/security_huks/issues/I4TYF1) | 【新增规格】HUKS支持key attestation和id attestation. | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 32 | [I4TYEM](https://gitee.com/openharmony/security_huks/issues/I4TYEM)| 【新增规格】HUKS支持安全等级凭据的导入签发及验证 | 安全子系统 | SIG_Security | [@chaos-liang](https://gitee.com/Chaos-Liang) |
| 33   | [I4H4FR](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4H4FR)|【distributed_kv_store】支持多用户数据隔离和共享|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 34   | [I4RGFY](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4RGFY)|【DataShare】基于ExtensionAbility新框架重构并提供单设备上跨应用数据共享能力|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 35   | [I4UZJ6](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4UZJ6)|【资料】提供分布式数据对象能力资料跟踪需求|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 36   | [I4UZK0](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4UZK0)|【资料】RDB提供分布式关系型数据库|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 37   | [I4UZL4](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4UZL4)|【资料】data_share提供对数据访问方式的控制|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 38   | [I4V038](https://gitee.com/openharmony/security_access_token/issues/I4V038)  | 【新增规格】实现通过应用权限管理界面设置应用权限    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 39   | [I4V032](https://gitee.com/openharmony/security_access_token/issues/I4V032)  | 【新增规格】应用权限权限管理界面实现    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 40   | [I4V02Y](https://gitee.com/openharmony/security_access_token/issues/I4V02Y)  | 【新增规格】主体设备上应用卸载时同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 41   | [I4V02P](https://gitee.com/openharmony/security_access_token/issues/I4V02P)  | 【DFX】Token信息的dump机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 42   | [I4V02K](https://gitee.com/openharmony/security_access_token/issues/I4V02K)  | 【新增规格】主体设备上应用授权状态更新同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 43   | [I4TYDA](https://gitee.com/openharmony/security_access_token/issues/I4TYDA)  | 【新增规格】token信息跨设备同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 44   | [I4TYCV](https://gitee.com/openharmony/security_access_token/issues/I4TYCV)  | 【新增规格】设备上线时的native进程的token信息同步    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 45   | [I4V03D](https://gitee.com/openharmony/security_selinux/issues/I4V03D)  | 【新增特性】支持SELinux开关的dump机制    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 46   | [I4JBF2](https://gitee.com/openharmony/account_os_account/issues/I4JBF2)  | [账号子系统]os_account_standard部件分布式组网账号管理需求    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 47   | [I4IU5W](https://gitee.com/openharmony/account_os_account/issues/I4IU5W)  | [帐号子系统]os_account_standard部件本地多用户生命周期管理需求    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 48   | [I4V74F](https://gitee.com/openharmony/communication_dsoftbus/issues/I4V74F)  | [新增特性]【组网】拓扑管理    | 软总线子系统 | SIG_SoftBus | [@fanxiaoyu321](https://gitee.com/fanxiaoyu321)       |
| 49   | [I4Q6B5](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B5)|【SR000GIILK:【资料】hitrace部件的js api需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 50   | [I4PJE4](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE4)|【SR000GI6P9:【资料】hiprofiler部件资料需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|

## OpenHarmony 3.1.5.1版本转测试信息：

| ********转测试版本号：    OpenHarmony 3.1.5.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第4轮测试，验收:    |
| L0L1:                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-2-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221247/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221247-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221124/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221124-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221047/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221047-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-2-20**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221317/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221317-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.1/20220219_231339/L2-SDK-MAC.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220221_201100/version-Master_Version-OpenHarmony_3.1.5.1-20220221_201100-dayu200.tar.gz |

## OpenHarmony 3.1.5.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4NJTS](https://gitee.com/openharmony/ace_engine_lite/issues/I4NJTS) | [轻量级图形子系统]支持通用touch事件、list组件支持scrollbottom/scrolltop事件 | 轻量系统       | SIG_AppFramework         | [@piggyguy](https://gitee.com/piggyguy)               |
| 2    | [I4NJTD](https://gitee.com/openharmony/graphic_ui/issues/I4NJTD) | [轻量级图形子系统]list组件支持scrollbottom/scrolltop事件     | 轻量系统       | SIG_AppFramework         | [@piggyguy](https://gitee.com/piggyguy)               |
| 3    | [I4OGF0](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4OGF0) | 【资料】thermal_manager部件类资料需求                        | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4OEQT](https://gitee.com/openharmony/powermgr_power_manager/issues/I4OEQT) | 【集成验证】不亮屏检测                                       | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 5    | [I4P7EY](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7EY) | [语言编译运行时子系统]支持TS Runtime                         | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 6    | [I4P7F5](https://gitee.com/openharmony/third_party_musl/issues/I4P7F5) | [语言编译运行时子系统]支持LLDB命令窗口                       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 7    | [I4P7EV](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4P7EV) | [语言编译运行时子系统]支持ts/js模块化编译                    | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 8    | [I4OGD1](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD1?from=project-issue) | 【新增特性】【DMS】支持组件间跨设备的onCall调用              | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 9   | [I4OGCY](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCY?from=project-issue) | 【新增特性】【AMS/框架】支持组件间跨设备的onCall调用         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 10   | [I4OGCX](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCX?from=project-issue) | 【新增特性】【任务管理】快照变化更新                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 11   | [I4OGCW](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCW?from=project-issue) | 【新增特性】【任务管理】任务变化更新                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 12   | [I4OGCV](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCV?from=project-issue) | 【新增特性】【任务管理】同步任务快照                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 13   | [I4OGCU](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCU?from=project-issue) | 【新增特性】【任务管理】结束任务同步                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 14   | [I4OGCT](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCT?from=project-issue) | 【新增特性】【任务管理】发起任务同步                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 15   | [I4OGCS](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCS?from=project-issue) | 【新增特性】【任务管理】查询任务快照                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 16   | [I4OGCP](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCP?from=project-issue) | 【新增特性】【DMS】跨设备组件权限校验                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 17   | [I4OGCK](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCK?from=project-issue) | 【增强特性】框架等待分布式对象同步完成后返回迁移结果         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 18   | [I4OH99](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH99?from=project-issue) | 【samgr】注册/查询本地系统服务权限控制                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 19   | [I4TS0Z](https://gitee.com/openharmony/distributedschedule_samgr_lite/issues/I4TS0Z?from=project-issue) | 【新增】轻量系统samgr支持远程服务管理                            | 轻量系统      | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 20   | [I4OH96](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH96?from=project-issue) | 【samgr】samgr资料                                           | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 21   | [I4OH93](https://gitee.com/openharmony/device_profile_core/issues/I4OH93?from=project-issue) | 【device_profile】同步功能适配可信群组                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 22   | [I4OGD7](https://gitee.com/openharmony/device_profile_core/issues/I4OGD7?from=project-issue) | 【device_profile】deviceProfile资料                          | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 23   | [I4PBBV](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBBV) | 【新增特性】事件耗时调用                                     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 24   | [I4PD0O](https://gitee.com/openharmony/notification_ans_standard/issues/I4PD0O) | 【特性增强】通知发送使能能力增强                             | 事件通知子系统 | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 25   | [I4PBQ1](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBQ1) | 【增强特性】分布式通知能力支持dump命令                       | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 26   | [I4PBR0](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBR0) | 【新增特性】支持其他设备的通知点击后在跨设备跳转             | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 27  | [I4PC2S](https://gitee.com/openharmony/notification_ces_standard/issues/I4PC2S) | 【新增特性】公共事件支持多用户特性                           | 事件通知子系统 | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 28  | [I4PCHK](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHK) | 【新增特性】C++/JS 客户端与服务端互通                        | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 29 | [I4PCS0](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS0) | 【新增特性】提供Ability启动停止方法                          | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 30  | [I4PQ5F](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5F) | 【资料】提供ZIDL部件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 31  | [I4PQ5N](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5N) | 【增强特性】支持IAbilityController                           | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 32  | [I4PCNK](https://gitee.com/openharmony/aafwk_standard/issues/I4PCNK) | 【资料】提供运行管理新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 33  | [I4PCPA](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPA) | 【资料】提供环境变化新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 34  | [I4PCV0](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV0) | 【资料】提供通用组件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 35  | [I4PQ5X](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5X) | 【增强特性】Extension接口整改                                | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 36  | [I4PCLY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLY) |【新增特性】对外接口权限校验验收|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 37  | [I4PCPI](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPI) |【增强特性】支持系统环境查询|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 38  | [I4PCRG](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRG) |【新增特性】提供Ability监听器|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 39 | [I4PCRL](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRL) |【新增特性】测试框架整体功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 40  | [I4PCRO](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRO) |【新增特性】测试框架需要提供如下查询相关的功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 41  | [I4PCRQ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRQ) |【新增特性】提供调度组件生命周期相关的功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 42  | [I4PCRX](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRX) |【新增特性】测试框架提供可以执行shell命令能力|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 43  | [I4PCS6](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS6) |【新增特性】AA适配测试框架|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 44  | [I4PCVU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVU) |【新增特性】通用组件call调用|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 45  | [I4PKYA](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYA?from=project-issue) | 【新增特性】包命令行工具                                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 46  | [I4PKYS](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYS?from=project-issue) | 【新增特性】支持现有js查询接口的权限管控                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 47  | [I4PKYW](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYW?from=project-issue) | 【新增特性】支持现有js监听安装、卸载、更新状态变化接口的权限管控 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 48  | [I4PKYX](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYX) | 【新增特性】支持现有js清理缓存接口的权限管控                 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 49 | [I4PKYY](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYY) | 【新增特性】支持现有js使能/禁用应用/组件接口的权限管控       | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 50  | [I4PKYZ](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYZ) | 【新增特性】返回应用类型                                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 51  | [I4PKZ1](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKZ1) | 【新增特性】应用数据目标标签设置                             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 52  | [I4PKYT](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYT) | 【新增特性】支持现有js安装接口的权限管控 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 53  | [I4QA3D](https://gitee.com/openharmony/appexecfwk_standard/issues/I4QA3D?from=project-issue) | 【增强特性】新增zlib解压、压缩数据native接口 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 54  | [I4SY5T](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5T?from=project-issue) | 【SysCap】包管理子系统支持SysCap机制 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 55  | [I4Q9S6](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9S6)  | [内核子系统]【增强特性】支持内核接口融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 56 | [I4QESV](https://gitee.com/openharmony/device_manager/issues/I4QESV)  | 【新增特性】设备可用状态上报性能跟踪   | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)           |
| 57   | [I4QEKZ](https://gitee.com/openharmony/drivers_framework/issues/I4QEKZ) | 【新增特性】支持用户态平台驱动接口                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 58 | [I4QEL1](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL1) | 【新增特性】【新增特性】支持基于FB显示架构的基础功能                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 59  | [I4QEL2](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL2) | 【增强特性】马达驱动模型能力增强                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 60   | [I4QT3S](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3S) | 【新增特性】长时任务管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 61   | [I4QT3T](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3T) | 【新增特性】长时任务规范 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 62   | [I4QT3U](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3U) | 【新增特性】长时任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 63   | [I4QT3V](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3V) | 【新增特性】长时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 64   | [I4QT3W](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3W) | 【新增特性】长时任务申请/注销 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 65   | [I4QU0O](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0O) | 【新增特性】设备和系统相关状态检测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 66   | [I4QU0P](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0P) | 【新增特性】延迟任务调度 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 67   | [I4QU0R](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0R) | 【新增特性】延迟任务后台管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 68   | [I4QU0S](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0S) | 【新增特性】延迟任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 69  | [I4QU0T](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0T) | 【新增特性】延迟任务适配PC多账户 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 70   | [I4U076](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4U076) | 【SysCap】全局资源调度管控子系统支持SysCap机制 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 71  | [I4R2Y1](https://gitee.com/openharmony/global_cust_lite/issues/I4R2Y1) | 【增强特性】提供定制框架js接口                               | 标准系统 | SIG_ApplicationFramework          | [@zhengbin5](https://gitee.com/zhengbin5)         |
| 72  | [I4R2Y8](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2Y8) | 【新增特性】支持颜色模式限定词                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 73  | [I4R2YA](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YA) | 【新增特性】新增资源管理NDK接口                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 74  | [I4R2YC](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YC) | 【新增特性】资源overlay                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 75  | [I4R2MP](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2MP) | 【新增特性】时区数据编译                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 76  | [I4R2LF](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2LF) | 【新增特性】时区数据解析                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 77  | [I4R3FR](https://gitee.com/openharmony/global_i18n_standard/issues/I4R3FR) | 【资料】全球化新增API资料更新                       | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 78  | [I4RD3H](https://gitee.com/openharmony/kernel_liteos_m/issues/I4RD3H?from=project-issue) | 【liteos_m部件】增加支持POSIX接口                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5) |
| 79  | [I4QEKY](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKY) | 【新增特性】Audio支持模拟耳机设备                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
|  80  | [I4RTYX](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYX?from=project-issue) | 【新增特性】【接口】提供查询本机或者组网内其它设备的设备安全等级信息的接口 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  81  | [I4RTYW](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYW?from=project-issue) | 【新增特性】【服务】支持获取自己或者组网内其它设备的设备安全等级信息 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  82  | [I4RTYU](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYU?from=project-issue) | 【新增特性】【服务】支持被组网内其它设备查询自己的设备安全等级信息 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  83  | [I4RTYN](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYN?from=project-issue) | 【新增特性】【南向】支持OEM厂家接入设备安全等级模块 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
| 84 | [I4SY5E](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5E?from=project-issue) | 【packing_tool部件】支持打包工具将syscap二进制文件打包到hap包中 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 85 | [I4SY5G](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5G?from=project-issue) | 【bundle_manager部件】支持基于SysCap的应用安装 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 86    | [I4TNBV](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBV) | 【新增规格】进程启动配置能力增强                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 87    | [I4TNBL](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBL) | 【新增特性】支持UHDF类进程按需启动                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 88    | [I4TNBQ](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBQ) | 【SysCap】启动恢复子系统支持SysCap机制                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 89    | [I4HAMD](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMD) | 【data_share_ability】支持对数据访问方式的控制                    | 标准系统   | SIG_DataManagement | [@verystone](https://gitee.com/verystone)         |
| 90    | [I4TJFZ](https://gitee.com/openharmony/security_deviceauth/issues/I4TJFZ) | 【增强特性】DeviceAuth部件支持设备间互信关系认证的多用户隔离，使用指定系统用户下管理的互信关系进行认证                   | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 91    | [I4TJG1](https://gitee.com/openharmony/security_deviceauth/issues/I4TJG1) | 【增强特性】DeviceAuth部件实现互信群组数据多实例，支持指定用户的数据查询                    | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 92    | [I4TSJE](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSJE) |  【新增规格】【face_auth】支持用户本地人脸录入   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 93    | [I4TSJY](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSJY) |  【新增规格】【face_auth】支持用户本地人脸认证   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 94    | [I4TSK7](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSK7) |  【新增规格】【face_auth】支持用户本地人脸删除   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 95    | [I4TSKP](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSKP) |  【需求描述】独立需求跟踪人脸认证框架DFX相关需求   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 96    | [I4SAI0](https://gitee.com/openharmony/security_dataclassification/issues/I4SAI0) |  【新增特性】提供DataTransitMgrLib部件，支持数据跨设备流转时的管控策略   | 标准系统   | SIG_Security      | [@wangyongzhong2](https://gitee.com/wangyongzhong2)     |
| 97   | [I4H4FH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4H4FH)|【distributed_kv_store】分布式数据库支持分类分级|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 98   | [I4H3M8](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3M8)|【新增特性】分布式数据对象支持复杂类型|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 99   | [I4HAMD](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMD)|【data_share_ability】支持对数据访问方式的控制|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 100   | [I4PO00](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4PO00)|【分布式RDB】数据同步需求|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 101   | [I4OTW6](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4OTW6)|【distributed_kv_store】分布式数据库Query支持InKeys谓词|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 102   | [I4TXSP](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4TXSP)|【SysCap】分布式数据管理子系统支持SysCap机制|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 103   | [I4TTK5](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTK5)|【新增特性】支持磁盘管理查询的特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 104   | [I4TTJV](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJV)|【新增特性】支持卷信息查询和管理特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 105   | [I4TTJN](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJN)|【新增特性】支持外卡设备相关事件分发特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 106   | [I4TTJJ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJJ)|【资料】storage_manag   er部件资料需求|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 107   | [I4TTHQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTHQ)|【新增特性】支持外部存储访问需求|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 108   | [I4TTHF](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTHF)|【新增特性】支持外卡上下线管理特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 109   | [I4TTGR](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTGR)|【新增特性】【storage_manager部件】文件加密特性使能|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 110   | [I4TTH9](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTH9)|【新增特性】【storage_manager部件】CE密钥生命周期管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 111   | [I4TT7Y](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TT7Y)|【新增特性】【storage_manager部件】文件加密特性对外接口|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 112   | [I4SNSU](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4SNSU)|【新增特性】支持应用沙箱隔离能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 113   | [I4TTM7](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTM7)|【新增特性】支持设备间的sendfile能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 114   | [I4TTOU](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOU)|【新增特性】支持提供用户态跨设备分享接口能力规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 115   | [I4TTOP](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOP)|【新增特性】支持建立本地文件与分布式文件的映射规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 116   | [I4TTOJ](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOJ)|【新增特性】为数据标签访问提供应用接口|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 117   | [I4TTOF](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOF)|【新增特性】支持异账号能力规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 118   | [I4TTNG](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTNG)|【新增特性】支持数据分类设备分级，控制数据流转规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 119   | [I4TTN8](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTN8)|【新增特性】支持分布式文件系统的基础功能|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 120   | [I4TTMN](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTMN)|【新增特性】支持应用包名级权限配置|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 121 | [I4TT8L](https://gitee.com/openharmony/security_huks/issues/I4TT8L) |【新增规格】HUKS提供三段式密钥管理接口|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 122 | [I4TT8P](https://gitee.com/openharmony/security_huks/issues/I4TT8P)|【新增规格】HUKS 架构优化，提供抽象统一的HAL层。|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 123 | [I4TT8Z](https://gitee.com/openharmony/security_huks/issues/I4TT8Z)|【新增规格】HUKS支持多用户隔离|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 124 | [I4S5RB](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5RB) |【新增特性】PKI应用签名工具支持生成Profile签名调试/发布证书|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 125 | [I4S5R7](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5R7) |【新增特性】PKI应用签名工具支持生成应用调试/发布证书|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 126 | [I4S5RC](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5RC) |【新增特性】PKI应用签名工具支持Profile文件签名|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 127 | [I4S5QZ](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5QZ) |【新增特性】PKI应用签名工具支持Hap包签名|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 128   | [I4U08D](https://gitee.com/openharmony/build/issues/I4U08D) | [语言编译运行时]【SysCap】语言编译器运行时子系统支持SysCap  | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 129   | [I4P7F7](https://gitee.com/openharmony/third_party_musl/issues/I4P7F7) | [语言编译运行时，图形图像，DRF]NDK整体集成 | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 130   | [I4QQ1E](https://gitee.com/openharmony/ark_runtime_core/issues/I4QQ1E) | [语言编译运行时]【新增规格】Pandafile模块中zlib替换miniz | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 131   | [I4TMTO]() | [语言编译运行时]【c_cpp_runtime部件】动态库卸载 | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 132   | [I4U73N](https://gitee.com/openharmony/filemanagement_file_api/issues/I4U73N)|【SysCap】文件管理子系统支持SysCap机制|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 133   | [I4Q6AQ](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AQ)|【【新增特性】Watchdog机制标准系统|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 134   | [I4Q6AP](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AP)|【新增特性】支持JS app性能分析信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 135   | [I4Q6AN](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AN)|【新增特性】：命令行调试通道|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 136   | [I4PJE6](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE6)|【【新增特性】支持JS app内存信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 137   | [I4PJE5](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE5)|【新增特性】支持JS app native内存信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 138   | [I4PJDO](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDO)|【新增特性】hungtask卡死-内核hungtask监控D状态进程|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 139   | [I4PJDN](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDN)|【新增特性】SCREEN ON卡死-不亮屏故障卡死|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 140   | [I4PJDP](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDP)|【新增特性】Power按键-长按、短按Power按键事件上报|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 141   | [I4PJDM](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDM)|【新增特性】INIT卡死-init长时间不执行|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 142   | [I4Q6AR](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AR)|【新增特性】鸿蒙芯片维测框架|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 143   | [I4Q6AZ](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6AZ)|【资料】hilog部件的js api需求，kmsg支持需求，init日志支持需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 144   | [I4Q6B2](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B2)|【新增特性】hilog支持开机阶段的日志打印和保存|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 145   | [I4Q6B4](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B4)|【新增特性】hilog支持js接口|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 146   | [I4Q6BA](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6BA)|【增强特性】支持HiLog关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 147   | [I4Q6B9](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B9)|【增强特性】支持HiSysEvent关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 148   | [I4Q6B7](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B7)|【增强特性】支持HiAppEvent关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 149   | [I4Q6B6](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B6)|【增强特性】支持HiTrace JS接口|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 150   | [I4U0JZ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4U0JZ)|【新增特性】供鸿蒙hisysevent系统事件管理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 151   | [I4PUKP](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4PUKP)|【新增特性】hisysevent订阅接口支持TAG方式订阅事件|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 152   | [I4U0KL](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4U0KL)|【SysCap】DFX子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 153   | [I4U0KP](https://gitee.com/openharmony/developtools_profiler/issues/I4U0KP)|【profiler部件】cpu profiler功能|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 154   | [I4U89V](https://gitee.com/openharmony/xts_acts/issues/I4U89V)|【新增】驱动/输入驱动XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 155   | [I4U8I3](https://gitee.com/openharmony/xts_acts/issues/I4U8I3)|【新增】智能开关面板样例|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 156   | [I4U8KB](https://gitee.com/openharmony/xts_acts/issues/I4U8KB)|【新增】驱动/显示驱动适配|轻量系统|SIG_Driver|[@zianed](https://gitee.com/zianed)|
| 157   | [I4U8LZ](https://gitee.com/openharmony/xts_acts/issues/I4U8LZ)|【新增】驱动/显示驱动XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 158   | [I4U8N4](https://gitee.com/openharmony/xts_acts/issues/I4U8N4)|【新增】驱动/输入驱动适配|轻量系统|SIG_Driver|[@zianed](https://gitee.com/zianed)|
| 159   | [I4U8O1](https://gitee.com/openharmony/xts_acts/issues/I4U8O1)|【新增】芯片架构SoC与Board解耦端到端联调|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 160   | [I4U8VP](https://gitee.com/openharmony/xts_acts/issues/I4U8VP)|【新增】编译构建/Module rules 编译构建流程适配SoC与Board分离机制|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 161   | [I4U8WL](https://gitee.com/openharmony/xts_acts/issues/I4U8WL)|【新增】硬件芯片/hisilico跟随SoC与Board分离机制进行调整|轻量系统|SIG_DevBoard|[@northking-super](https://gitee.com/northking-super)|
| 162   | [I4S878](https://gitee.com/openharmony/xts_acts/issues/I4S878)|【新增】用户程序框架/轻设备包管理 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 163   | [I4TY9D](https://gitee.com/openharmony/ace_engine_lite/issues/I4TY9D)|【新增】 轻量系统图形/UI组件和图形基础组件 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 164   | [I4TFTB](https://gitee.com/openharmony/drivers_framework/issues/I4TFTB)|【关联】轻量系统新增HCS宏式解析接口|轻量系统|SIG_Driver|[@fx_zhang](https://gitee.com/fx_zhang)|
| 165   | [I4T1JA](https://gitee.com/openharmony/xts_acts/issues/I4T1JA)|【新增】轻量系统分布式软总线/分布式软总线 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 166   | [I4TS0Z](https://gitee.com/openharmony/distributedschedule_samgr_lite/issues/I4TS0Z)|【新增】轻量系统samgr支持远程服务管理|轻量系统|SIG_DistributedSchedule|[@zjucx](https://gitee.com/zjucx)|



## OpenHarmony 3.1.3.5版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第4轮测试，验收:    |
| L0L1:                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-1-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202253/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202253-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202242/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202242-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202340/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202340-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-30**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202311/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202311-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.5/20220129_203628/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220130_193126/version-Master_Version-OpenHarmony_3.1.3.5-20220130_193126-hispark_taurus_L2.tar.gz|
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220130_193317/version-Master_Version-OpenHarmony_3.1.3.5-20220130_193317-dayu200.tar.gz |


**需求列表:**
| no   | issue                                                        | feture description                           | platform       | sig                      | owner                                             |
| :--- | ------------------------------------------------------------ | :------------------------------------------- | :------------- | :----------------------- | :------------------------------------------------ |
| 1    | [I4P7F1](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7F1) | [语言编译运行时子系统]方舟支持列号显示       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)     |
| 2    | [I4PC3R](https://gitee.com/openharmony/aafwk_standard/issues/I4PC3R) | 【新增特性】提供卡片开发基础能力             | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181) |
| 3    | [I4PCGJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCGJ) | 【资料】提供卡片框架新增/增强特性资料说明    | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181) |
| 4    | [I4PCO3](https://gitee.com/openharmony/aafwk_standard/issues/I4PCO3) | 【新增特性】主线程EventRunner耗时检测        | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)     |
| 5    | [I4PPZF](https://gitee.com/openharmony/aafwk_standard/issues/I4PPZF) | 【增强特性】卡片框架适配配置文件变更         | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 6    | [I4PQ0I](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0I) | 【增强特性】获取启动参数                     | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 7    | [I4PQ0K](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0K) | 【增强特性】扩展Extension独立进程运行        | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 8    | [I4PQ0M](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0M) | 【增强特性】上下文提供消息发送和监听能力     | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 9    | [I4PQ0Z](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0Z) | 【新增特性】支持导出客户端应用信息           | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 10   | [I4PQ12](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ12) | 【新增特性】支持导出AMS信息                  | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 11   | [I4PQ13](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ13) | 【增强特性】上下文提供权限校验及权限申请接口 | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 12   | [I4PQ19](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ19) | 【新增特性】支持桌面进程异常恢复             | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 13   | [I4PQ1E](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ1E) | 【增强特性】支持常驻进程异常恢复             | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 14   | [I4PQ1O](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ1O) | 【新增特性】支持NewWant                      | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)           |
| 15   | [I4PKYG](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYG) | 【新增特性】支持关键流程hitrace              | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 16   | [I4PKYK](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYK) | 【增强特性】启动扫描                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 17   | [I4SIH9](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SIH9) | 【新增特性】应用申请权限管理                 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 18   | [I4PBSZ](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSZ) | 【新增特性】根据设备状态决策通知是否提醒     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)           |
| 19  | [I4QESE](https://gitee.com/openharmony/device_manager/issues/I4QESE)  | 【新增特性】PIN码认证过程中的界面实现    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)
| 20  | [I4QESK](https://gitee.com/openharmony/device_manager/issues/I4QESK)  | 【部件化专项】【device_manager部件】device_manager部件标准化   | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)
| 21    | [I4QEKW](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKW) | 【新增特性】提供codec设备驱动模型，支持codec类型设备                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 22    | [I4QEKX](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKX) | 【特性增强】pipeline模块能力增强                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 23    | [I4QT44](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QT44) | 【新增特性】短时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 24   | [I4R2CV](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2CV) | 【增强特性】大小写转换                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 25   | [I4R2X9](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2X9) | 【部件化专项】全球化子系统部件标准化                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 26   | [I4R3CG](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R3CG) | 【新增特性】资源预览优化                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 27   | [I4OH95](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH95?from=project-issue) | 【部件化专项】系统服务管理子系统部件标准化                   | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 28   | [I4RD2M](https://gitee.com/openharmony/kernel_liteos_m/issues/I4RD2M?from=project-issue) | 【部件化专项】LiteOS-M内核部件标准化                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)
| 29   | [I4RD2U](https://gitee.com/openharmony/kernel_liteos_a/issues/I4RD2U?from=project-issue) | 【部件化专项】LiteOS-A内核部件标准化                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)
| 30   | [I4R6SK](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4R6SK)|【部件化专项】【key_value_store部件】key_value_store部件标准化|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 31    | [I4QEKQ](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKQ) | 【新增特性】Display的Gralllo、Gfx和Device的HDI接口实现服务                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 32    | [I4QEKS](https://gitee.com/openharmony/drivers_framework/issues/I4QEKS) | 【新增特性】platform_driver部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 33    | [I4QEKT](https://gitee.com/openharmony/drivers_framework/issues/I4QEKT) | 【新增特性】peripheral_driver部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 34    | [I4QEKU](https://gitee.com/openharmony/drivers_framework/issues/I4QEKU) | 【新增特性】driver_framework部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 35  | [I4RTX4](https://gitee.com/openharmony/security_device_security_level/issues/I4RTX4)              | 【部件化专项】设备安全等级管理（DSLM）部件标准             | 标准系统        | SIG_Security                      | [@zhirenx](https://gitee.com/zhirenx)                    |
| 36   | [I4P7EX](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7EX) | [语言编译运行时子系统]DFX维测支持运行时支持DFX/在异常时需要hook机制以及栈列信息特性 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 37   | [I4RFWF](https://gitee.com/openharmony/security_deviceauth/issues/I4RFWF) | 【部件化专项】【deviceauth部件】deviceauth部件标准化 | 标准系统       | SIG_Security       | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 38   | [I4S36A](https://gitee.com/openharmony/js_util_module/issues/I4S36A) | [语言编译运行时子系统]【部件化专项】ts_js_common_api部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
|  39  | [I4S36C](https://gitee.com/openharmony/third_party_musl/issues/I4S36C) | [语言编译运行时子系统]【部件化专项】cpp_compiler_toolchain部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 40   | [I4S36D](https://gitee.com/openharmony/ark_js_runtime/issues/I4S36D) | [语言编译运行时子系统]【部件化专项】ark_ide_support部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 41   | [I4S36F](https://gitee.com/openharmony/ark_ts2abc/issues/I4S36F) | [语言编译运行时子系统]【部件化专项】ark_frontend_compiler部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 42   | [I4S36G](https://gitee.com/openharmony/ark_js_runtime/issues/I4S36G) | [语言编译运行时子系统]【部件化专项】ark_runtime部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 43    | [I4RXK2](https://gitee.com/openharmony/startup_init_lite/issues/I4RXK2) | 【新增特性】支持为进程代持fd                      | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 44    | [I4RXJM](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJM) | 【新增特性】支持进程根据热插拔事件按需启动                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 45    | [I4RXJ9](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJ9) | 【新增特性】支持socket类进程按需启动                               | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 46    | [I4RXJ2](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJ2) | 【新增规格】统一init维护命令                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 47    | [I4S6QC](https://gitee.com/openharmony/useriam_user_auth/issues/I4S6QC) | 【资料】用户IAM子系统介绍资料需求                                | 用户IAM子系统 | SIG_Security       | [@wangxu](https://gitee.com/wangxu43)     |
| 48   | [I4LRGQ](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LRGQ)  | [内核子系统]【新增特性】OpenHarmony内核基线使能     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 49   | [I4RU58](https://gitee.com/openharmony/security_dataclassification/issues/I4RU58)   | 【部件化专项】【data_transit_mgr_lib部件】data_transit_mgr_lib部件标准化  | 标准系统   | SIG_Security  | [@wangyongzhong2](https://gitee.com/wangyongzhong2)  |

## OpenHarmony 3.1.3.3版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第3轮测试，验收:    |
| L0L1: 不涉及                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-1-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165641/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165641-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165549/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165549-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165605/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165605-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-20**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_192244/version-Master_Version-OpenHarmony_3.1.3.3-20220120_192244-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.3/20220120_233918/L2-SDK-MAC.tar.gz  |
| hi3516dv300-L2版本： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165824/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165824-hispark_taurus_L2.tar.gz
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165650/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165650-dayu200.tar.gz |

**已解决的ISSUE单列表：**
无

**需求列表:**
| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4GYCD](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYCD) | 【新增特性】支持软件耗电统计                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I4GYCN](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYCN) | 【新增特性】支持硬件耗电统计                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4GYDQ](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYDQ) | 【新增特性】支持耗电详情记录                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4GY9U](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GY9U) | 【新增特性】支持内核温控服务                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 5    | [I4GYAF](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GYAF) | 【新增特性】支持用户层和服务温控服务                         | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 6    | [I4P7FB](https://gitee.com/openharmony/third_party_musl/issues/I4P7FB) | [语言编译运行时子系统]提供NDK中HOS与OHOS两种target，ABI的clang/llvm编译工具链 提供NDK中调试工具链，支持lldb，asan等功能 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 7    | [I4P7F2](https://gitee.com/openharmony/third_party_musl/issues/I4P7F2) | [语言编译运行时子系统]支持基于lldb的断点调试                 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 8    | [I4P7F3](https://gitee.com/openharmony/third_party_musl/issues/I4P7F3) | [语言编译运行时子系统]支持C/C++应用调试栈/变量的查看能力     | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 9    | [I4P7EQ](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7EQ) | [语言编译运行时子系统]ts类型信息提取                         | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 10   | [I4P7ER](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7ER) | [语言编译运行时子系统]Ts2abc中的类型信息增强                 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 11   | [I4P7ET](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7ET) | [语言编译运行时子系统]Panda file中的类型系统存储             | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 12   | [I4P7EU](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7EU) | [语言编译运行时子系统]abc支持列号信息                        | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 13  | [I4P7FY](https://gitee.com/openharmony/js_util_module/issues/I4P7FY) | [语言编译运行时子系统]container特性/LightWeightMap接口规格   | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 14  | [I4P7FX](https://gitee.com/openharmony/js_util_module/issues/I4P7FX) | [语言编译运行时子系统]container特性/Deque接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 15  | [I4P7FV](https://gitee.com/openharmony/js_util_module/issues/I4P7FV) | [语言编译运行时子系统]container特性/HashSet接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 16  | [I4P7FT](https://gitee.com/openharmony/js_util_module/issues/I4P7FT) | [语言编译运行时子系统]container特性/TreeSet接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 17  | [I4P7FR](https://gitee.com/openharmony/js_util_module/issues/I4P7FR) | [语言编译运行时子系统]container特性/TreeMap接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 18  | [I4P7FP](https://gitee.com/openharmony/js_util_module/issues/I4P7FP) | [语言编译运行时子系统]container特性/Queue接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 19 | [I4P7FO](https://gitee.com/openharmony/js_util_module/issues/I4P7FO) | [语言编译运行时子系统]container特性/Vector接口规格           | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 20  | [I4P7FM](https://gitee.com/openharmony/js_util_module/issues/I4P7FM) | [语言编译运行时子系统]container特性/PlainArray接口规格       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 21  | [I4P7FL](https://gitee.com/openharmony/js_util_module/issues/I4P7FL) | [语言编译运行时子系统]container特性/ArrayList接口规格        | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 22 | [I4P7FK](https://gitee.com/openharmony/js_util_module/issues/I4P7FK) | [语言编译运行时子系统]container特性/LightWeightSet接口规格   | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 23 | [I4P7FH](https://gitee.com/openharmony/js_util_module/issues/I4P7FH) | [语言编译运行时子系统]container特性/HashMap接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 24  | [I4P7FF](https://gitee.com/openharmony/js_util_module/issues/I4P7FF) | [语言编译运行时子系统]container特性/List接口规格             | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 25 | [I4P7FE](https://gitee.com/openharmony/js_util_module/issues/I4P7FE) | [语言编译运行时子系统]container特性/Stack接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 26  | [I4OGCO](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCO?from=project-issue) | 【新增特性】【DMS】提供跨设备迁移接口                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 27  | [I4OGCL](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCL?from=project-issue) | 【增强特性】【框架】迁移数据保存                             | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 28  | [I4OH9B](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH9B?from=project-issue) | 【samgr】动态加载未启动的本地系统服务                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 29 | [I4OH9A](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH9A?from=project-issue) | 【samgr】系统服务启动性能跟踪                                | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 30  | [I4OH98](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH98?from=project-issue) | 【samgr】SAMGR异常恢复                                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 31  | [I4OH94](https://gitee.com/openharmony/device_profile_core/issues/I4OH94?from=project-issue) | 【device_profile】校验DP客户端访问profile记录的权限          | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 32  | [I4OGD6](https://gitee.com/openharmony/device_profile_core/issues/I4OGD6?from=project-issue) | 【部件化专项】分布式DeviceProfile子系统部件标准化            | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 33  | [I4PBJF](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBJF) | 【部件化专项】distributed_notification_service部件标准化     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 34  | [I4PBNF](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBNF) | 【部件化专项】common_event部件标准化                         | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 35  | [I4PBO1](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBO1) | 【资料】通知能力开发者材料                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 36  | [I4PBPM](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBPM) | 【增强特性】分布式通知支持流控                               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 37  | [I4PBRM](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBRM) | 【新增特性】支持其他设备的通知点击后在本设备跳转             | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 38  | [I4PBRW](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBRW) | 【新增特性】支持设备级的分布式通知使能控制                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 39 | [I4PBSE](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSE) | 【新增特性】支持通知管理应用设置和查询应用级的分布式通知使能 | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 40  | [I4PBSP](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSP) | 【新增特性】支持应用设置分布式通知能力是否使能               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 41  | [I4PBT7](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBT7) | 【新增特性】分布式通知同步                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 42 | [I4PBU3](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBU3) | 【新增特性】分布式通知联动取消                               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 43 | [I4PBUU](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBUU) | 【新增规格】 支持通过config.json静态配置公共事件，支持通过wokscheduler静态拉起订阅者 | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 44 | [I4PBV9](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBV9) | 【新增规格】 支持静态订阅者管控                              | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 45 | [I4PCH4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCH4) | 【新增特性】卡片支持多用户                                   | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 46 | [I4PCHC](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHC) | 【元能力-运行管理部件化】基于SysCap的平台代码解耦和部件化改造 | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 47 | [I4PCHF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHF) | 【元能力-卡片管理部件化】基于SysCap的平台代码解耦和部件化改造 | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 48  | [I4PPVU](https://gitee.com/openharmony/aafwk_standard/issues/I4PPVU) | 【新增特性】进程多实例                                       | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 49 | [I4PPW2](https://gitee.com/openharmony/aafwk_standard/issues/I4PPW2) | 【资料】提供测试框架新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 50  | [I4PCLO](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLO) | 【增强特性】Ability多实例                                    | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 51  | [I4PCM1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM1) | 【新增特性】提供ce/de级上下文                                | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 52  | [I4PCOQ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCOQ) | 【新增特性】应用管理                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 53  | [I4PCS2](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS2) | 【资料】提供测试工具新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 54  | [I4PCVN](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVN) | 【新增特性】支持任务快照获取和更新                           | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 55  | [I4PCWF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCWF) | 【资料】提供服务组件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 56  | [I4PPW6](https://gitee.com/openharmony/aafwk_standard/issues/I4PPW6) | 【增强特性】指定窗口模式启动组件                             | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 57  | [I4PPWA](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWA) | 【增强特性】Ability框架适配配置文件变更                      | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 58 | [I4PPWD](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWD) | 【增强特性】Extension框架适配配置文件变更                    | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 59 | [I4PKY3](https://gitee.com/openharmony/aafwk_standard/issues/I4PKY3) | 【部件化专项】bundle_manager部件标准化                       | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 60  | [I4PKYB](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYB) | 【增强特性】schema适配配置文件重构                           | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 61  | [I4PKYD](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYD) | 【新增特性】安装能力适配config.json调整                      | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 62  | [I4PKYF](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYF) | 【新增特性】支持查询指定Metadata资源profile配置文件的信息    | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 63  | [I4PKYH](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYH) | 【新增特性】支持对Extension的查询                            | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 64  | [I4PKYI](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYI) | 【新增特性】提供清除数据的能力                               | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 65  | [I4PKYR](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYR) | 【新增特性】系统定义权限的初始化                             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 66 | [I4PKYU](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYU) | 【新增特性】支持对应用权限信息的查询                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 67 | [I4PXN0](https://gitee.com/openharmony/build/issues/I4PXN0)  | [编译构建子系统]【增强特性】编译构建日志优化，日志按级别显示               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 68 | [I4PXND](https://gitee.com/openharmony/build/issues/I4PXND)  | [编译构建子系统]【增强特性】hb命令安装、集成及扩展支持               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 69 | [I4PKY7](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY7) | 【新增特性】跨设备信息同步 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 70 | [I4PKY8](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY8) | 【新增特性】跨设备信息查询 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 71 | [I4PKYE](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYE) | 【新增特性】支持查询禁用的组件信息和应用信息 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 72 | [I4QA2V](https://gitee.com/openharmony/appexecfwk_standard/issues/I4QA2V) | 【部件化专项】bundle_tool部件标准化 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 73 | [I4Q9RC](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9RC)  | [内核子系统]【增强特性】支持指令集融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5) |
| 74 | [I4PZE7](https://gitee.com/openharmony/device_manager/issues/I4PZE7)  | 【增强特性】支持周边不可信设备的发现    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)   |
| 75 | [I4PZE4](https://gitee.com/openharmony/device_manager/issues/I4PZE4)  | 【增强特性】支持JSAPI接口    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)  |
| 76 | [I4PZE2](https://gitee.com/openharmony/device_manager/issues/I4PZE2)  | 【新增特性】支持多用户切换    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 77 | [I4PZDZ](https://gitee.com/openharmony/device_manager/issues/I4PZDZ)  | 【增强特性】支持账号无关设备的PIN码认证    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 78 | [I4PZDY](https://gitee.com/openharmony/device_manager/issues/I4PZDY)  | 【增强特性】支持可信设备列表查询    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 79 | [I4PZDT](https://gitee.com/openharmony/device_manager/issues/I4PZDT)  | 【增强特性】支持可信设备的上下线监听    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 80 | [I4PZC9](https://gitee.com/openharmony/device_manager/issues/I4PZC9)  | 【新增特性】支持分布式设备管理接口授权控制    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 81 | [I4PZC7](https://gitee.com/openharmony/device_manager/issues/I4PZC7)  | 【新增特性】支持设备被发现开关控制    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)   |
| 82 | [I4QESH](https://gitee.com/openharmony/device_manager/issues/I4QESH)  | 【新增特性】设备Id的查询和转换    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)|
| 83  | [I4QEKP](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKP) | 【新增特性】基于HDF驱动框架提供light驱动能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 84   | [I4QEKR](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKR) | 【新增特性】Display-Layer、Display-Gralloc、Display-Gfx针对轻量系统的增强参考实现                          | 轻量系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 85   | [I4QEKV](https://gitee.com/openharmony/usb_manager/issues/I4QEKV) | 【新增特性】USB服务 HDI接口实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 86   | [I4OEOZ](https://gitee.com/openharmony/powermgr_power_manager/issues/I4OEOZ) | 【新增特性】监控输入亮屏输入事件，并根据输入事件进行亮、灭屏   | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 87   | [I4MBRM](https://gitee.com/openharmony/powermgr_power_manager/issues/I4MBRM) | 【新增特性】支持接近光控制锁，通话时通过接近光控制亮灭屏的特性   | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 88   | [I4MBRL](https://gitee.com/openharmony/powermgr_power_manager/issues/I4MBRL) | 【新增特性】支持显示相关的能耗调节    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 89   | [I4QGI0](https://gitee.com/openharmony/powermgr_power_manager/issues/I4QGI0) | 【新增特性】长按power Key弹出关机界面    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 90   | [I4QGJH](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4QGJH) | 【部件化专项】thermal_manager部件标准化    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 91   | [I4QGLI](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4QGLI) | 【部件化专项】battery_statistics部件标准化    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 92   | [I4QT3R](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3R) | 【部件化专项】全局资源调度管控子系统部件标准化 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 93   | [I4QT3Y](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT3Y) | 【新增特性】支持系统进程统一代理三方提醒 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 94   | [I4QT40](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT40) | 【新增特性】提醒后台代理计时能力 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 95   | [I4QT41](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT41) | 【新增特性】提醒代理管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 96   | [I4QT42](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT42) | 【新增特性】提醒代理相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 97   | [I4QT43](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QT43) | 【新增特性】全局资源调度框架 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 98   | [I4QU0N](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QU0N) | 【新增特性】支持Soc调频 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 99   | [I4QU0V](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0V) | 【新增特性】支持短时任务申请/注销/查询 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 100   | [I4QU0W](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0W) | 【新增特性】短时任务后台管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 101   | [I4QU0X](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0X) | 【新增特性】短时任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 102   | [I4QU0Z](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0Z) | 【新增特性】短时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 103  | [I4R2M3](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2M3) | 【新增特性】时区数据部署                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 104  | [I4R2C2](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2C2) | 【新增特性】多偏好语言                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 105  | [I4R2YF](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YF) | 【增强特性】ResourceManager适配hap包结构和配置清单文件调整                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 106  | [I4R3DO](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R3DO) | 【增强特性】restool工具适配配置清单文件调整                                   | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 107  | [I4OWTZ](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4OWTZ)  | [内核子系统]【外部依赖】内核实现进程的tokenID设置     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 108  | [I4QE9K](https://gitee.com/openharmony/utils/issues/I4QE9K)  | [内核子系统]【新增特性】提供内核态驱动与用户态之间、用户态与用户态之间的内核共享能力     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 109  | [I4QM8F](https://gitee.com/openharmony/kernel_linux_build/issues/I4QM8F)  | [内核子系统]【部件化专项】Linux内核部件标准化     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 110  | [I4Q79P](https://gitee.com/openharmony/communication_ipc/issues/I4Q79P)  | [新增特性]【RPC】RPC支持跨设备收发及死亡通知 | 软总线子系统 | SIG_SoftBus | [@pilipala195](https://gitee.com/pilipala195)       |
| 111 | [I4Q79C](https://gitee.com/openharmony/communication_ipc/issues/I4Q79C)  | [新增特性]【RPC】RPC支持跨设备服务管理 | 软总线子系统 | SIG_SoftBus | [@pilipala195](https://gitee.com/pilipala195)       |
| 112 | [I4IIRC](https://gitee.com/openharmony/communication_ipc/issues/I4IIRC)  | [新增特性]【RPC】IPC实现tokenid的传递和查询 | 软总线子系统 | SIG_SoftBus | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao)       |
| 113 | [I4RCE2](https://gitee.com/openharmony/security_selinux/issues/I4RCE2)  | 【部件化专项】【selinux部件】部件标准化     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 114 | [I4RCDU](https://gitee.com/openharmony/security_selinux/issues/I4RCDU)  | 【新增规格】支持只读镜像的文件的标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 115 | [I4RCDS](https://gitee.com/openharmony/security_selinux/issues/I4RCDS)  | 【新增规格】支持native进程标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 116 | [I4RCD9](https://gitee.com/openharmony/security_selinux/issues/I4RCD9)  | 【新增规格】支持SELinux虚拟文件标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 117 | [I4RCD6](https://gitee.com/openharmony/security_selinux/issues/I4RCD6)  | 【新增规格】支持SELinux文件标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 118 | [I4RCD0](https://gitee.com/openharmony/security_selinux/issues/I4RCD0)  | 【新增特性】支持SELinux策略加载和使能     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 119 | [I4RCBT](https://gitee.com/openharmony/security_selinux/issues/I4RCBT)  | 【新增规格】提供hap应用selinux domain设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 120 | [I4RCB7](https://gitee.com/openharmony/security_selinux/issues/I4RCB7)  | 【新增规格】提供hap应用数据目录的selinux标签设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 121 | [I4RCAA](https://gitee.com/openharmony/security_selinux/issues/I4RCAA)  | 【新增特性】实现文件系统二级目录的selinux标签设置    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 122 | [I4JBEK](https://gitee.com/openharmony/account_os_account/issues/I4JBEK)  | [帐号子系统]支持分布式组网账号ID的派生    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 123 | [I4JBFB](https://gitee.com/openharmony/account_os_account/issues/I4JBFB)  | [账号子系统]支持分布式组网账号状态管理    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 124 | [I4IU33](https://gitee.com/openharmony/account_os_account/issues/I4IU33)  | [帐号子系统]支持本地多用户功能设置与内容修改    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 125 | [I4JBFI](https://gitee.com/openharmony/account_os_account/issues/I4JBFI)  | [账号子系统]支持本地多用户分布式信息查询    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 126 | [I4IU3V](https://gitee.com/openharmony/account_os_account/issues/I4IU3V)  | [帐号子系统]支持域账户和本地用户关联    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 127 | [I4IU6A](https://gitee.com/openharmony/account_os_account/issues/I4IU6A)  | [帐号子系统]支持本地用户约束条件配置    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 128 | [I4IU6N](https://gitee.com/openharmony/account_os_account/issues/I4IU6N)  | [帐号子系统]支持本地多用户基础信息管理    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 129 | [I4IU74](https://gitee.com/openharmony/account_os_account/issues/I4IU74)  | [帐号子系统]支持本地用户的创建和删除    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 130 | [I4RCRT](https://gitee.com/openharmony/usb_manager/issues/I4RCRT) | [驱动子系统]SR000GNFHL：【新增特性】USB服务 部件标准化                          | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen)           |
| 131 | [I4RBA4](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBA4)  | [新增特性]PKI应用签名工具支持生成签名密钥 | 安全子系统       | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)      |
| 132 | [I4RBEN](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBEN)  | [新增特性]PKI应用签名工具支持生成证书签名请求（CSR）| 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)      |
| 133 | [I4RBEA](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBEA)  | [新增特性]PKI应用签名工具支持生成CA密钥和证书 | 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)       |
| 134 | [I4RFJP](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RFJP)  | [部件化专项]【PKISignCentre部件】PKISignCentre部件标准化 | 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)       |
| 135 | [I4PNX7](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4PNX7)|【分布式RDB】数据存储需求|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 136 | [I4R6T4](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4R6T4)|【部件化专项】【native_appdatamgr部件】native_appdatamgr部件标准化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 137 | [I4RFYC](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4RFYC)|【部件化专项】【objectstore部件】分布式数据对象部件标准化|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 138 | [I4H3LS](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3LS)|分布式数据对象提供JS接口|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 139 | [I4NUD5](https://gitee.com/openharmony/ark_js_runtime/issues/I4NUD5)|方舟C++ FFI支持继承关系|语言编译运行时子系统|SIG_CompileRuntime|[@weng-changcheng](https://gitee.com/weng-changcheng)|
| 140 | [I4P86T](https://gitee.com/openharmony/js_worker_module/issues/I4P86T)|支持Worker中可以再创建Worker，子Worker可以跟父Worker通信|语言编译运行时子系统|SIG_CompileRuntime|[@weng-changcheng](https://gitee.com/weng-changcheng)|
| 141 | [I4P7FN](https://gitee.com/openharmony/js_util_module/issues/I4P7FN)|【新增规格】container特性/LinkedList接口规格|语言编译运行时子系统|SIG_CompileRuntime|[@gongjunsong](https://gitee.com/gongjunsong)|
| 142 | [I4RG4R](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG4R)  | 【DFX】用户IAM框架DFX需求 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 143 | [I4RG4X](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG4X)  | 【user_idm】支持用户本地人脸的删除 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 144 | [I4RG55](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG55)  | 【user_idm】支持用户本地认证凭据信息查询 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 145 | [I4RG59](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG59)  | 【user_idm】支持用户本地口令的录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 146 | [I4RG5G](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5G)  | 【user_idm】支持用户本地口令的删除 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 147 | [I4RG5M](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5M)  | 【user_idm】支持用户本地人脸的录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 148 | [I4RG5R](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5R)  | 【user_idm】支持删除用户时，删除该用户的身份认证凭据 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 149 | [I4RGMX](https://gitee.com/openharmony/useriam_user_idm/issues/I4RGMX)  | 【部件化专项】【user_idm部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 150 | [I4RG8D](https://gitee.com/openharmony/useriam_user_auth/issues/I4RG8D)  | 【user_auth】支持用户本地口令认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 151 | [I4RG7W](https://gitee.com/openharmony/useriam_user_auth/issues/I4RG7W)  | 【user_auth】支持用户本地人脸认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 152 | [I4RGNO](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGNO)  | 【部件化专项】【pin_auth部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 153 | [I4RG9E](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG9E)  | 【DFX】口令认证框架DFX需求 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 154 | [I4RG91](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG91)  | 【pin_auth】支持用户本地口令认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 155 | [I4RG8W](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG8W)  | 【pin_auth】支持用户本地口令录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 156 | [I4RGU3](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGU3)  | 【pin_auth】提供软实现| 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 157 | [I4RCRM](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRM)|【IDE工具支持】交互事件回调耗时打印|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 158 | [I4RCRL](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRL)|【IDE工具支持】渲染流水线耗时打印|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 159 | [I4RCRK](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRK)|【DFX】ACE框架超时检测机制|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 160 | [I4RCRI](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRI)|【新增规格】卡片支持鼠标悬停事件|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 161 | [I4RCRH](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRH)|【新增特性】自定义builder|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 162 | [I4RCRG](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRG)|【新增特性】$$双向绑定编译转换支持|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)||
| 163 | [I4RCRF](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRF)|【新增特性】新增自定义组件支持访问子组件数据|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 164 | [I4RCRE](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRE)|【新增特性】新增NAPI继承机制|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 165 | [I4RCRD](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRD)|【新增规格】新增OffscreenCanvas支持抗锯齿特性|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 166 | [I4RCRC](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRC)|【新增特性】样式状态编译转换支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 167 | [I4RCRA](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRA)|【新增特性】ArkUI对接窗口新架构|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 168 | [I4Q8ZH](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZH)|【跟踪】【hiappevent部件】应用事件功能增强|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 169 | [I4RCR0](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4RCR0)|【跟踪】【hilog部件】流水日志功能增强|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 170 | [I4Q6AS](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AS)|【资料】faultloggerd部件 南北向文档需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 171 | [I4NJTS](https://gitee.com/openharmony/ace_engine_lite/issues/I4NJTS) |[轻量级图形子系统]支持通用touch事件、list组件支持scrollbottom/scrolltop事件|轻量系统|SIG_AppFramework|[@piggyguy](https://gitee.com/piggyguy)|
| 172 | [I4NJTD](https://gitee.com/openharmony/graphic_ui/issues/I4NJTD) |[轻量级图形子系统]list组件支持scrollbottom/scrolltop事件|轻量系统|SIG_AppFramework|[@piggyguy](https://gitee.com/piggyguy)|
| 173 | [I4RFBD](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RFBD)|【新增特性】【local_file_system】支持fat/exfat/ntfs等可插拔文件系统能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 174 | [I4RDNG](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RDNG)|【新增特性】【local_file_system】支持ext4/f2fs等用户态工具的能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 175 | [I4RE2G](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RE2G)|【新增特性】【local_file_system】支持ext4/f2fs格式镜像打包能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 176 | [I4RENG](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RENG)|【新增特性】【local_file_system】支持ext4/f2fs文件系统开机resize和fsck|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 177 | [I4RF6Z](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RF6Z)|【新增特性】【local_file_system】支持fat/exfat/ntfs等可插拔文件系统能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 178 | [I4RFEQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RFEQ)|【新增特性】【storage_service部件】支持密钥存储管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 179 | [I4RG9F](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RG9F)|【新增特性】【storage_service】CE/DE文件软加密策略管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 180 | [I4PW8P](https://gitee.com/openharmony/build/issues/I4PW8P)|【新增特性】支持生成部件列表和部件依赖关系|编译构建子系统|SIG_CompileRuntime|@烈烈(https://gitee.com/xiaolielie)|
| 181   |[I4ROL2](https://gitee.com/openharmony/applications_call/issues/I4ROL2) |【通话】-通话中支持DTMF键盘 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 182   |[I4ROL0](https://gitee.com/openharmony/applications_call/issues/I4ROL0) |【通话】-通话中显示状态及计时 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 183   |[I4ROKZ](https://gitee.com/openharmony/applications_call/issues/I4ROKZ) |【通话】-来电支持通话页面拉起| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 184   |[I4ROKY](https://gitee.com/openharmony/applications_call/issues/I4ROKY) |【通话】-通话中显示联系人号码和姓名 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 185   |[I4ROLD](https://gitee.com/openharmony/applications_call/issues/I4ROLD) |【通话】-来电显示通知 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 186   |[I4ROLG](https://gitee.com/openharmony/applications_call/issues/I4ROLG) |【短信】- 短信 - 短信接收 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 187   |[I4ROKU](https://gitee.com/openharmony/applications_mms/issues/I4ROKU) |【短信】- 短信 - 短信接收 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 188   |[I4QKLL](https://gitee.com/openharmony/applications_mms/issues/I4QKLL) |【短信】- 短信 - 短信单发（单卡）| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 189   |[I4QKLK](https://gitee.com/openharmony/applications_mms/issues/I4QKLK) |【短信】- 短信 - 短信送达报告 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|

## OpenHarmony 3.1.3.2版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第2轮测试，验收:    |
| L0L1: 不涉及                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-1-13**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162158/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162158-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162321/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162321-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162233/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162233-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-13**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162702/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162702-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.2/20220112_173827/L2-SDK-MAC.tar.gz    |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220113_215637/version-Master_Version-OpenHarmony_3.1.3.2-20220113_215637-dayu200.tar.gz |

**已解决的ISSUE单列表：**
无

**需求列表:**
| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4QGKM](https://gitee.com/openharmony/powermgr_battery_manager/issues/I4QGKM) | 【部件化专项】battery_manage部件标准化 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I412F4](https://gitee.com/openharmony/powermgr_power_manager/issues/I412F4) | 【省电模式】 支持省电模式                                    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4GYBV](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GYBV) | 【新增特性】提供温升监控接口                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4P7FC](https://gitee.com/openharmony/third_party_musl/issues/I4P7FC) | [语言编译运行时子系统]提供NDK所需的musl版本的libc基础库以及相应头文件 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 5    | [I4P7F9](https://gitee.com/openharmony/third_party_musl/issues/I4P7F9) | [语言编译运行时子系统]提供NDK所需的libc++库以及相应头文件    | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 6    | [I4OGCN](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCN?from=project-issue) | 【增强特性】【DMS】根据指定设备发起迁移能力，接收迁移结果    | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 7    | [I4OGCM](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCM?from=project-issue) | 【新增特性】【任务管理】提供获取实时任务接口  | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 8    | [I4PBOK](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBOK) | 【新增特性】通知支持多用户                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 9    | [I4PCM4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM4) | 【新增特性】上下文提供应用/Hap包/组件信息查询能力            | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 10   | [I4PCM8](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM8) | 【新增特性】应用启动/组件切换流程trace                       | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 11   | [I4PCNZ ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCP6) | 【新增特性】服务组件泄漏检测                                 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 12   | [I4PCPP](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPP) | 【新增特性】上下文适配多用户                                 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 13   | [I4PCPR](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPR) | 【新增特性】非并发模式下，禁止非当前用户的应用通过其他方式自启 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 14   | [I4PCPV](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPV) | 【新增特性】提供指定用户启动组件的系统接口                   | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 15   | [I4PCQ1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQ1) | 【新增特性】提供指定用户管理应用的系统接口                   | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 16   | [I4PCQJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQJ) | 【新增特性】对外接口适配多用户                               | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 17   | [I4PCQP](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQP) | 【新增特性】支持singleuser的运行模式                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 18   | [I4PCQU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQU) | 【新增特性】启动初始化默认用户                               | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 19   | [I4PCQW](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQW) | 【新增特性】启动用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 20   | [I4PCQY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQY) | 【新增特性】切换用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 21   | [I4PCR2](https://gitee.com/openharmony/aafwk_standard/issues/I4PCR2) | 【新增特性】停止用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 22   | [I4PBP7](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBP7)|【新增特性】支持应用发送模板通知（调试能力）|事件通知子系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 23   | [I4PBPE](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBPE)|【新增特性】支持进度条通知|事件通知子系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 24   | [I4PCGY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCGY)|【增强特性】新增卡片开发基类|元能力子系统|SIG_ApplicationFramework|[@lsq1474521181](https://gitee.com/lsq1474521181)|
| 25   | [I4PCH9](https://gitee.com/openharmony/aafwk_standard/issues/I4PCH9)|【增强特性】通过配置文件配置服务卡片|元能力子系统|SIG_ApplicationFramework|[@lsq1474521181](https://gitee.com/lsq1474521181)|
| 26   | [I4PCLL](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLL)|【新增特性】JS提供的应用级别上下文|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 27   | [I4PCLN](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLN)|【新增特性】Abilty的状态恢复|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 28   | [I4PCM6](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM6)|【新增特性】提供应用或组件状态监听/查询能力|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 29   | [I4PCP1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCP1)|【新增特性】应用运行信息查询|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 30   | [I4PCPG](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPG)|【增强特性】支持系统环境变化通知|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 31   | [I4PCR8](https://gitee.com/openharmony/aafwk_standard/issues/I4PCR8)|【增强特性】支持常驻进程开机启动|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 32   | [I4PCSB](https://gitee.com/openharmony/aafwk_standard/issues/I4PCSB)|【新增特性】强制停止进程|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 33   | [I4PCV4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV4)|【新增特性】支持任务切换|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 34   | [I4PCV9](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV9)|【新增特性】多任务管理|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 35   | [I4PCVA](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVA)|【新增特性】支持任务锁|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 36   | [I4PCVF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVF)|【新增特性】支持任务清除|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 37   | [I4PCVZ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVZ)|【新增特性】支持指定displayId启动Ability|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 38   | [I4PCW3](https://gitee.com/openharmony/aafwk_standard/issues/I4PCW3)|【增强特性】pendingwant机制支持跨设备启动通用组件|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 39  | [I4PKYL](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYL) | 【新增特性】支持查询指定用户下的应用信息                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 40  | [I4PKYM](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYM) | 【新增特性】支持多用户创建                                   | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 41  | [I4PKYN](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYN) | 【新增特性】支持多用户删除                                   | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 42  | [I4PKYO](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYO) | 【新增特性】支持安装应用到指定用户                           | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 43  | [I4PKYP](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYP) | 【新增特性】支持卸载指定用户下的应用                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 44  | [I4PKY8](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PO7Z) | 【新增特性】installd 提供应用空间统计、cache统计             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 45  | [I4PC12](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4PC12) | 支持基本文件异步操作API需求                                  | 标准系统       | SIG_Kernel               | [@panqinxu](https://gitee.com/panqinxu)               |
| 46  | [I4PXMP](https://gitee.com/openharmony/build/issues/I4PXMP)  | [编译构建子系统]【增强特性】统一编译入口，轻量级和标准系统使用hb可编译 | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 47  | [I4PXNS](https://gitee.com/openharmony/build/issues/I4PXNS)  | [编译构建子系统]【新增特性】提供NDK的编译模板               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 48  | [I4PXNZ](https://gitee.com/openharmony/build/issues/I4PXNZ)  | [编译构建子系统]【新增特性】 Native-SDK中提供cmake toolchain文件               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 49  | [I4PXRD](https://gitee.com/openharmony/build/issues/I4PXRD)  | [编译构建子系统]【新增特性】归一的部件定义和编译               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 50  | [I4JBFF](https://gitee.com/openharmony/account_os_account/issues/I4JBFF)  | [账号子系统]【新增特性】支持本地多用户信息查询               | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 51  | [I4IU2T](https://gitee.com/openharmony/account_os_account/issues/I4IU2T)  | [账号子系统]【新增特性】支持本地多用户订阅及取消订阅          | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 52  | [I4IU3B](https://gitee.com/openharmony/account_os_account/issues/I4IU3B)  | [账号子系统]【新增特性】支持本地多用户启动、停止、切换动作     | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 53  | [I4Q9QZ](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9QZ)  | [内核子系统]【增强特性】支持南向接口融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 54  | [I4Q8F8](https://gitee.com/openharmony-sig/useriam_faceauth/issues/I4Q8F8)  | [用户IAM子系统]【新增特性】提供L2人脸识别框架     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 55  | [I4Q8F5](https://gitee.com/openharmony-sig/useriam_faceauth/issues/I4Q8F5)  | [用户IAM子系统]【新增特性】提供L2人脸录入控件     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 56  | [I4Q8F3](https://gitee.com/openharmony-sig/useriam_faceauth/issues/I4Q8F3)  | [用户IAM子系统]【新增特性】提供L2统一身份认证JS接口     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 57  | [I4LKQ0](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LKQ0)  | [内核子系统]【新增特性】cpuset与cpu热插拔解耦     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 58  | [I4HAMI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMI)  | 【data_share_ability】支持跨应用订阅数据库的变化    | 标准系统 | SIG_DataManagement | [@verystone](https://gitee.com/verystone)      |
| 59   | [I4Q6AS](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AS)|【新增特性】FreezeDetector|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 60   | [I4Q6AV](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AV)|【新增特性】在Openharmony上hiview插件管理平台代理加载特性|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 61   | [I4Q6AT](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AT)|【新增特性】在Openharmony上FaultLogger添加js api|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 62   | [I4Q6B3](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B3)|【新增特性】支持内核日志的读取和落盘|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 63   | [I4Q6B1](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B1)|【增强特性】将hilogd NDK库加入到OpenHarmony NDK包|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 64   | [I4Q8ZL](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZL)|【增强特性】日志管理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 65   | [I4Q8ZK](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZK)|【增强特性】进程异常信号处理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 66   | [I4Q8ZJ](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZJ)|【增强特性】抓取调用栈工具|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 67   | [I4Q8ZI](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZI)|【新增特性】抓取调用栈基础库|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 68    | [I4QEKH](https://gitee.com/openharmony/drivers_framework/issues/I4QEKH) | 【新增特性】提供共享内存相关HDI能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 69    | [I4QEKI](https://gitee.com/openharmony/drivers_framework/issues/I4QEKI) | 【新增特性】驱动开发工具支持标准系统驱动开发                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 70    | [I4QEKJ](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKJ) |【新增特性】HDI接口适配linux-input驱动                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 71    | [I4QEKM](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKM) | 【新增特性】提供power HDI接口能力                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 72    | [I4QEKK](https://gitee.com/openharmony/drivers_framework/issues/I4QEKK) | 【新增特性】基于HDF驱动框架提供硬件TIMER驱动                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 73    | [I4QEKL](https://gitee.com/openharmony/drivers_framework/issues/I4QEKL) | 【新增特性】基于HDF驱动框架构建统一的平台驱动对象模型                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 74    | [I4QEKN](https://gitee.com/openharmony/usb_manager/issues/I4QEKN) | 【新增特性】USB Device功能实现                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 75    | [I4QEKO](https://gitee.com/openharmony/usb_manager/issues/I4QEKO) | 【新增特性】USB Host功能实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 76   | [I4QEPQ](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4QEPQ)|【资料】【RDB】支持QuerySql返回结果集，进一步支持更广泛的查询方式|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 77   | [I4NZP6](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4NZP6)|【RDB】增加多表查询能力|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 78   | [I4FZ6B](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4FZ6B)|【RDB】提供事务能力|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 79   | [I4HAMI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMI)|【data_share_ability】支持跨应用订阅数据库的变化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 80   | [I4QC4U](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4U)|【新增特性】PC预览资源管理特性对接全球化规格|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 81   | [I4QC4S](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4S)|【新增规格】文本计时器组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 82   | [I4QC4R](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4R)|【新增规格】进度条组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 83   | [I4QC4P](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4P)|【新增规格】文字时钟组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 84   | [I4QC4N](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4N)|【新增规格】TextInput组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 85   | [I4QC4O](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4O)|【新增规格】Select组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 86   | [I4QC4K](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4K)|【新增规格】TextArea组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 87    | [I4QGHF](https://gitee.com/openharmony/powermgr_power_manager/issues/I4QGHF) | 【部件化专项】power_manage部件标准化 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |


## OpenHarmony 3.1.3.1版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第1轮测试，验收:   |
| L0L1: 不涉及                                      |
| L2:                               |
| **API****变更：**：                |
| **L0L1****转测试时间：2022-1-6**                          |
| **L0L1****转测试版本获取路径：**                            |
| hispark_pegasus版本:   http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_092223/version-Master_Version-OpenHarmony_3.1.3.1-20220106_092223-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_092343/version-Master_Version-OpenHarmony_3.1.3.1-20220106_092343-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_092109/version-Master_Version-OpenHarmony_3.1.3.1-20220106_092109-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-6**                            |
| **L2****转测试版本获取路径：**                              |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_205029/version-Master_Version-OpenHarmony_3.1.3.1-20220106_205029-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.1/20220106_235728/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_160053/version-Master_Version-OpenHarmony_3.1.3.1-20220106_160053-hispark_taurus_L2.tar.gz |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_160117/version-Master_Version-OpenHarmony_3.1.3.1-20220106_160117-dayu200.tar.gz |

**已解决的ISSUE单列表：**
无

**需求列表:**
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I410YD](https://gitee.com/openharmony/powermgr_battery_manager/issues/I410YD) | 【充放电&Battery服务】 支持关机充电特性                     | 标准系统      | SIG_HardwareMgr         | [@aqxyjay](https://gitee.com/aqxyjay)                |
| 2   | [I410Y1](https://gitee.com/openharmony/powermgr_battery_manager/issues/I410Y1) | 【充放电&Battery服务】 电池温度异常关机保护                 | 标准系统      | SIG_HardwareMgr         | [@aqxyjay](https://gitee.com/aqxyjay)                |
| 3   | [I4OGD2](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD2?from=project-issue) | 【资料】跨设备组件调用新增/增强特性资料说明                 | 标准系统      | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)        |
| 4   | [I4PDB8](https://gitee.com/openharmony/drivers_framework/issues/I4PDB8) | 【新增特性】提供设备PnP事件监听接口                         | 标准系统      | SIG_HardwareMgr         | [@yuanbogit](https://gitee.com/yuanbogit)            |
| 5   | [I4PDAV](https://gitee.com/openharmony/applications_contacts/issues/I4PDAV) | 【联系人】通话记录 - 基本内容（名字、号码、时间、来电次数等） | 标准系统      | SIG_SystemApplication   | [@lv-zhongwei](https://gitee.com/lv-zhongwei)        |
| 6   | [I4PDAW](https://gitee.com/openharmony/applications_contacts/issues/I4PDAW) | 【联系人】通话记录 - 通话记录列表显示（TAB/列表展示）       | 标准系统      | SIG_SystemApplication   | [@lv-zhongwei](https://gitee.com/lv-zhongwei)        |
| 7    | [I4PDAY](https://gitee.com/openharmony/applications_systemui/issues/I4PDAY) | 【SystemUI】【状态栏】提示胶囊                               | 标准系统       | SIG_SystemApplication    | [@lv-zhongwei](https://gitee.com/lv-zhongwei)         |
| 8   | [I4PCX8](https://gitee.com/openharmony/communication_ipc/issues/I4PCX8?from=project-issue) | 【RPC】进程间IPC、设备间RPC支持HiTrace能力                  | 标准系统      | SIG_SoftBus             | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao)              |
| 9  | [I4PD3K](https://gitee.com/openharmony/startup_init_lite/issues/I4PD3K) | 进程退出后的回收处理策略配置能力增强                        | 标准系统      | SIG_BscSoftSrv          | [@xionglei16](https://gitee.com/xionglei16)          |
| 10  | [I4PD3C](https://gitee.com/openharmony/startup_init_lite/issues/I4PD3C) | 支持SA类进程按需启动                                        | 标准系统      | SIG_BscSoftSrv          | [@xionglei16](https://gitee.com/xionglei16)          |
| 11  | [I4NZVP](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4NZVP) | 【distributed_kv_store】提供分布式数据库JS API              | 标准系统      | SIG_DataManagement      | [@widecode](https://gitee.com/widecode)              |

## OpenHarmony 3.1.2.5(Beta)版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.5(Beta) *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第4轮测试，验收:    |
| L0L1: 不涉及                                       |
| L2: IT1~TI3全量需求验证                                     |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211230_193318/version-Master_Version-OpenHarmony_3.1.2.5-20211230_193318-hispark_pegasus_OpenHarmony-3.1-Beta.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211230_193358/version-Master_Version-OpenHarmony_3.1.2.5-20211230_193358-hispark_taurus_LiteOS_OpenHarmony-3.1-Beta.tar.gz |
| hispark_taurus_linux版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211230_193339/version-Master_Version-OpenHarmony_3.1.2.5-20211230_193339-hispark_taurus_Linux_OpenHarmony-3.1-Beta.tar.gz |
| **L2****转测试时间：2021-12-30**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_Beta/20220106_185129/version-Master_Version-OpenHarmony_3.1_Beta-20220106_185129-ohos-sdk_OpenHarmony-3.1-Beta.tar.gz |
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1_Beta/20220106_224324/L2-SDK-MAC.tar.gz    |
| hi3516dv300-L2版本：  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211231_191018/version-Master_Version-OpenHarmony_3.1.2.5-20211231_191018-hispark_taurus_L2_OpenHarmony-3.1-Beta.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211231_191442/version-Master_Version-OpenHarmony_3.1.2.5-20211231_191442-dayu200_OpenHarmony-3.1-Beta.tar.gz |

**2021/12/29已解决的ISSUE单列表：**
|serialNo| ISSUE                                                        | 问题描述                                                     |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|1||IDE配套问题，修改js版本号，否则IDE工具无法下载到SDK包，需要修改为1.0.6版本| 
|2||二级界面无法跳转，点击时间设置、亮度调节、音量调节等不生效问题| 
|3||状态栏依旧有Home/Back/功能键显示，但这些键点击无效| 
|4|I4O67X|标准系统_3516执行xts测试套ActsAnsDoNotDisturbTest报ace异常调用栈问题，影响社区流水线稳定性测试| 
|5|I4OBCG|libnotification.z.so文件NO Rpath/RunPath选项有问题| 
|6|I4ND5U|【openharmony】kr3568单板manager用例7失败| 

**2021/12/28已解决的ISSUE单列表：**
|serialNo| ISSUE                                                        | 问题描述                                                     |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|1|I4NEUX|执行xts测试套ActsBmsGetInfosTest报ace异常调用栈问题，影响社区流水线测试|
|2|I4O311|/system/bin/display-hotplug.sh脚本编译到正式版本中，疑似后门，应删除| 
|3||IDE配套问题，低代码工程适配失败| 
|4||EGG   心跳次数是三位数时，会文字显示不完整| 
|5||计算器 带小数乘除计算会有误差1.1*6=6.6000000000000006| 
|6||3516的性能不行导致绑定超时失败，client失败| 



**需求列表:**
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
|1|[I4M8FW](https://gitee.com/openharmony/account_os_account/issues/I4M8FF?from=project-issue)|【新增特性】支持应用账号基础信息管理|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|2|[I4MBQE](https://gitee.com/openharmony/miscservices_time/issues/I4MBQE)|新增特性：时间时区管理|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|3|[I4MBQF](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBQF)|【增强特性】【事件通知子系统】线程间EventHandler支持HiTrace能力|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|4|[I4MBQG](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQG?from=project-issue)|【资料】hisysevent部件资料需求|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|5|[I4MBQH](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQH?from=project-issue)|【新增特性】支持鸿蒙HiSysEvent部件提供查询接口|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|6|[I4MBQI](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQI?from=project-issue)|【新增特性】提供工具查询或者订阅系统事件|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|7|[I4MBQJ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQJ?from=project-issue)|【新增特性】支持鸿蒙HiSysEvent部件提供订阅接口|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|8|[I4MBQK](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I4MBQK?from=project-issue)|【资料】hiappevent部件资料需求|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|9|[I4MBQL](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I4MBQL?from=project-issue)|【新增特性】支持鸿蒙hiappevent部件的C接口|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|10|[I4MBQM](https://gitee.com/openharmony/build/issues/I4MBQM?from=project-issue)|【新增规格】L0-L2支持统一的部件配置|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|11|[I4MBQN](https://gitee.com/openharmony/build/issues/I4MBQN?from=project-issue)|【新增规格】L0-L2支持使用统一的编译命令进行编译|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|12|[I4MBQO](https://gitee.com/openharmony/build/issues/I4MBQO?from=project-issue)|【新增规格】L0-L2支持使用统一的编译流程|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|13|[I4MBQP](https://gitee.com/openharmony/build/issues/I4MBQP?from=project-issue)|【新增规格】L0-L2支持使用统一的gn模板|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|14|[I4MBQQ](https://gitee.com/openharmony/build/issues/I4MBQQ?from=project-issue)|【新增规格】L0-L2支持统一的产品配置|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|15|[I4MBQR](https://gitee.com/openharmony/build/issues/I4MBQR?from=project-issue)|【资料】制定gn编码规范和最佳实践指导|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|16|[I4MBQS](https://gitee.com/openharmony/account_os_account/issues/I4MBQS?from=project-issue)|【新增特性】支持应用账号信息查询|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|17|[I4MBQT](https://gitee.com/openharmony/account_os_account/issues/I4MBQT?from=project-issue)|【新增特性】支持应用账号功能设置与内容修改|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|18|[I4MBQU](https://gitee.com/openharmony/account_os_account/issues/I4MBQU?from=project-issue)|【新增特性】支持应用账号订阅及取消订阅|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|19|[I4MBQV](https://gitee.com/openharmony/account_os_account/issues/I4MBQV?from=project-issue)|【DFX】应用账号基础信息约束|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|20|[I4MBQW](https://gitee.com/openharmony/account_os_account/issues/I4MBQW?from=project-issue)|【新增特性】支持应用账号的新增和删除|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|21|[I4MBQX](https://gitee.com/openharmony/global_cust_lite/issues/I4MBQX?from=project-issue)|【增强特性】定制框架基础能力|标准系统|SIG_ApplicationFramework|[@zhengbin5](https://gitee.com/zhengbin5)|
|22|[I4MBQY](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBQY?from=project-issue)|【新增特性】资源编译工具支持增量编译|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|23|[I4MBQZ](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBQZ?from=project-issue)|【增强特性】时间段格式化|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|24|[I4MBR0](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR0?from=project-issue)|【增强特性】区域表示和属性|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|25|[I4MBR1](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR1?from=project-issue)|【增强特性】单复数支持|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|26|[I4MBR2](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR2?from=project-issue)|【增强特性】字符串排序|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|27|[I4MBR3](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR3?from=project-issue)|【增强特性】电话号码处理|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|28|[I4MBR4](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR4?from=project-issue)|【新增特性】字母表检索|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|29|[I4MBR5](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR5?from=project-issue)|【新增特性】度量衡体系和格式化|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|30|[I4MBR7](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR7?from=project-issue)|【新增特性】日历&本地历法|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|31|[I4MBR8](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR8?from=project-issue)|【增强特性】unicode字符属性|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|32|[I4MBR9](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR9?from=project-issue)|【增强特性】断词断行|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|33|[I4MBRA](https://gitee.com/openharmony/global_resmgr_standard/issues/I4MBRA?from=project-issue)|【新增特性】系统资源管理|标准系统|SIG_ApplicationFramework|[@jameshw](https://gitee.com/jameshw)|
|34|[I4MBRB](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBRB?from=project-issue)|【新增特性】rawfile资源管理|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|35|[I4MBRC](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRC?from=project-issue)|【hiperf部件】采样数据展示|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|36|[I4MBRD](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRD?from=project-issue)|【hiperf部件】性能数据采样记录|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|37|[I4MBRE](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRE?from=project-issue)|【hiperf部件】性能数据计数统计|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|38|[I4MBRF](https://gitee.com/openharmony/communication_wifi/issues/I4MBRF?from=project-issue)|【新增特性】支持STA基础特性的JS API接口|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|39|[I4MBRG](https://gitee.com/openharmony/communication_wifi/issues/I4MBRG?from=project-issue)|【新增特性】支持STA基础特性JS API资料文档|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|40|[I4MBRH](https://gitee.com/openharmony/communication_wifi/issues/I4MBRH?from=project-issue)|【新增特性】支持STA基础特性|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|41|[I4MBRI](https://gitee.com/openharmony/communication_wifi/issues/I4MBRI?from=project-issue)|【新增特性】支持SoftAP基础特性|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|42|[I4MBRJ](https://gitee.com/openharmony/communication_wifi/issues/I4MBRJ?from=project-issue)|【新增特性】提供WiFi模块的维测能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|43|[I4MBRK](https://gitee.com/openharmony/usb_manager/issues/I4MBRK?from=project-issue)|【新增特性】USB服务JS接口实现|标准系统|SIG_DistributedHardwareManagement|[@hhh2](https://gitee.com/hhh2)|
|44|[I4MBRP](https://gitee.com/openharmony/sensors_sensor/issues/I4MBRP?from=project-issue)|【泛Sensor】地磁场偏角和倾角|标准系统|SIG_DistributedHardwareManagement|[@hhh2](https://gitee.com/hhh2)|
|45|[I4MBRQ](https://gitee.com/openharmony/sensors_sensor/issues/I4MBRQ?from=project-issue)|【泛Sensor】地磁场偏角和倾角|标准系统|SIG_DistributedHardwareManagement|[@hhh2](https://gitee.com/hhh2)|
|45|[I4MBRR](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4MBRR?from=project-issue)|【资料】distributed_kv_store分布式数据库支持按谓词查询条件进行数据库记录的跨设备同步和订阅|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
|46|[I4MBRS](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4MBRS?from=project-issue)|【distributed_kv_store】分布式数据库支持按谓词查询条件进行数据库记录的跨设备同步和订阅|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
|47|[I4MBRT](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4MBRT?from=project-issue)|【资料】RDB提供数据库级安全加密|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|48|[I4MBRU](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4MBRU?from=project-issue)|【RDB】支持数据库加密|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|49|[I4MBRV](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRV?from=project-issue)|【samgr】系统服务状态监控|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|50|[I4MBRW](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRW?from=project-issue)|【samgr】服务进程内的SA名单管控|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|51|[I4MBRX](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRX?from=project-issue)|【samgr】加载指定系统服务|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|52|[I4MBRY](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRY?from=project-issue)|【samgr】系统服务进程管理|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|53|[I4MBRZ](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRZ?from=project-issue)|【samgr】全量服务列表初始化|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|54|[I4MBS0](https://gitee.com/openharmony/communication_dsoftbus/issues/I4MBS0?from=project-issue)|【新增特性】【组网】软总线支持网络切换组网|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|55|[I4MBS1](https://gitee.com/openharmony/communication_dsoftbus/issues/I4MBS1?from=project-issue)|【新增特性】【传输】软总线提供传输ExtAPI接口|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|56|[I4MBS2](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4MBS2?from=project-issue)|【新增特性】支持statfs API能力需求|标准系统|SIG_DataManagement|[@zhangzhiwi](https://gitee.com/zhangzhiwi)|
|57|[I4MBS3](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS3?from=project-issue)|【新增特性】支持长时任务通知|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|58|[I4MBS4](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS4?from=project-issue)|【新增特性】通知系统API支持权限管理|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|59|[I4MBS5](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS5?from=project-issue)|【新增特性】支持设置通知振动|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|60|[I4MBS6](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS6?from=project-issue)|【新增特性】支持通知声音设置和查询|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|61|[I4MBS7](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS7?from=project-issue)|【新增特性】通知支持免打扰|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|62|[I4MBS8](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS8?from=project-issue)|【新增特性】支持会话类通知|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|63|[I4MBS9](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBS9?from=project-issue)|【新增特性】EventHandler支持hitrace|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|64|[I4MBSA](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBSA?from=project-issue)|【新增特性】支持系统公共事件管理特性|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|65|[I4MBSB](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBSB?from=project-issue)|【新增特性】支持eventEmitter|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|66|[I4MBSC](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSC?from=project-issue)|【增强特性】支持Module和Ability的srcPath字段|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|67|[I4MBSD](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSD?from=project-issue)|【新增特性】支持多hap包安装|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|68|[I4MBSE](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSE?from=project-issue)|【新增特性】提供桌面包管理客户端|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|69|[I4MBSF](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSF?from=project-issue)|【新增特性】提供清除缓存数据js api|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|70|[I4MBSG](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSG?from=project-issue)|【增强特性】安装包信息查询|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|71|[I4MBSH](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSH?from=project-issue)|【新增特性】多hap安装时的签名校验|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|72|[I4MBSI](https://gitee.com/openharmony/aafwk_standard/issues/I4MBSI?from=project-issue)|【新增特性】ZIDL工具自动生成Extension C++服务端及客户端接口文件|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|73|[I4MBT4](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT4?from=project-issue)|【增强特性】支持常驻进程开机启动|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|74|[I4MBTN](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4MBTN?from=project-issue)|【新增特性】支持CMA复用特性|标准系统|SIG_Kernel|[@liuyoufang](https://gitee.com/liuyoufang)|
|75|[I4MBTO](https://gitee.com/openharmony/third_party_musl/issues/I4MBTO?from=project-issue)|【新增特性】支持内存占用分类查询|标准系统|SIG_CompileRuntime|[@huanghuijin](https://gitee.com/huanghuijin)|
|76|[I4MBTP](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTP?from=project-issue)|【增强特性】传感器驱动模型能力增强|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|77|[I4MBTQ](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTQ?from=project-issue)|【增强特性】传感器器件驱动能力增强|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|78|[I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR?from=project-issue)|【新增特性】Display-Layer HDI接口针对L2的参考实现；  Display-Gralloc HDI接口针对L2的参考实现；  Display-Device  HDI接口针对L2的参考实现；|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|79|[I4MBTS](https://gitee.com/openharmony/drivers_framework/issues/I4MBTS?from=project-issue)|【增强特性】 HDF-Input设备能力丰富|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|80|[I4MBTT](https://gitee.com/openharmony/drivers_framework/issues/I4MBTT?from=project-issue)|【新增特性】支持Linux/Liteos-a内核系统级休眠唤醒|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|81|[I4MBTU](https://gitee.com/openharmony/drivers_framework/issues/I4MBTU?from=project-issue)|【新增特性】支持同步/异步电源管理调用|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|82|[I4MBTV](https://gitee.com/openharmony/drivers_framework/issues/I4MBTV?from=project-issue)|【新增特性】提供hcs宏式解析接口|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|83|[I4MBU1](https://gitee.com/openharmony/applications_settings/issues/I4MBU1?from=project-issue)|【设置公共数据存储】Settings数据管理API|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|84|[I4MBU3](https://gitee.com/openharmony/applications_settings/issues/I4MBU3?from=project-issue)|【设置】系统-时间设置|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|85|[I4MBU5](https://gitee.com/openharmony/applications_settings/issues/I4MBU5?from=project-issue)|【设置】声音管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|86|[I4MBU6](https://gitee.com/openharmony/applications_settings/issues/I4MBU6?from=project-issue)|【设置】基础能力-数据管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|87|[I4MBU7](https://gitee.com/openharmony/applications_settings/issues/I4MBU7?from=project-issue)|【设置】基础能力-默认值管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|88|[I4MBU8](https://gitee.com/openharmony/applications_settings/issues/I4MBU8?from=project-issue)|【设置】基础能力-多设备形态差异化构建|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|89|[I4MBU9](https://gitee.com/openharmony/applications_systemui/issues/I4MBU9?from=project-issue)|【SystemUI】【通知】通知组件化|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|90|[I4MBUB](https://gitee.com/openharmony/ark_ts2abc/issues/I4MBUB?from=project-issue)|【新增特性】提供windows/MacOS/Linux的前端编译工具链|标准系统|SIG_CompileRuntime|[@godmiaozi](https://gitee.com/godmiaozi)|
|91|[I4MBUC](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUC?from=project-issue)|【新增特性】Openharmony jsapi替换为ark版本|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|92|[I4MBUD](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUD?from=project-issue)|【新增规格】内存管理分配回收功能/Concurrent mark算法以及Concurrent Sweep实现|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|93|[I4MBUE](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUE?from=project-issue)|【新增特性】OpenharmonyOS上默认内置应用替换为ark版本|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|94|[I4MBUF](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUF?from=project-issue)|【增强特性】Inline Cache功能|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|95|[I4MBUG](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUG?from=project-issue)|【增强特性】支持解释器call指令优化|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|96|[I4MBUH](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUH?from=project-issue)|【新增规格】CPU Profiler运行时实现|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|97|[I4MBUI](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUI?from=project-issue)|【新增规格】内存管理分配回收功能/ 支持Old Space的Partial GC|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|98|[I4MBUJ](https://gitee.com/openharmony/ark_ts2abc/issues/I4MBUJ?from=project-issue)|【新增特性】OpenHarmony应用工程编译构建能力  【描述】  1、在正常构建场景下，能够将开发者程序代码编译成方舟字节码  2、在编译出现错误时，输出准确编译错误提示信息|标准系统|SIG_CompileRuntime|[@godmiaozi](https://gitee.com/godmiaozi)|
|99|[I4MBUK](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUK?from=project-issue)|【新增规格】JS运行时支持预览器|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|100|[I4MBUL](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUL?from=project-issue)|【新增规格】方舟支持调试并且支持attach模式|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|101|[I4MBUM](https://gitee.com/openharmony/js_util_module/issues/I4MBUM?from=project-issue)|【新增规格】提供libc，c++，clang基础测试框架|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|102|[I4MBUN](https://gitee.com/openharmony/js_sys_module/issues/I4MBUN?from=project-issue)|【新增规格】支持utils特性  /提供process接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|103|[I4MBUO](https://gitee.com/openharmony/js_util_module/issues/I4MBUO?from=project-issue)|【新增规格】支持utils特性  /提供提供Scope接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|104|[I4MBUP](https://gitee.com/openharmony/js_util_module/issues/I4MBUP?from=project-issue)|【新增规格】支持utils特性  /提供提供Base64接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|105|[I4MBUQ](https://gitee.com/openharmony/js_util_module/issues/I4MBUQ?from=project-issue)|【新增规格】支持utils特性  /提供提供RationalNumber接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|106|[I4MBUR](https://gitee.com/openharmony/js_util_module/issues/I4MBUR?from=project-issue)|【新增规格】支持语言增强特性/提供JS Typeof 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|107|[I4MBUS](https://gitee.com/openharmony/js_api_module/issues/I4MBUS?from=project-issue)|【新增规格】支持URI特性 /提供 URI解析接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|108|[I4MBUT](https://gitee.com/openharmony/js_util_module/issues/I4MBUT?from=project-issue)|【新增规格】支持utils特性提供LRUBuffer接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|109|[I4MBUU](https://gitee.com/openharmony/js_api_module/issues/I4MBUU?from=project-issue)|【新增规格】支持XML特性/提供XmlPullParser 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|110|[I4MBUV](https://gitee.com/openharmony/js_api_module/issues/I4MBUV?from=project-issue)|【新增规格】支持XML特性/提供xmlSerializer 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|111|[I4MBUW](https://gitee.com/openharmony/js_api_module/issues/I4MBUW?from=project-issue)|【新增规格】支持XML特性/提供xml2JSObject 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|112|[I4MBUX](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUX?from=project-issue)|【新增规格】资源管理特性对接全球化规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|113|[I4MBUY](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUY?from=project-issue)|【新增规格】事件中增加Target获取尺寸|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|114|[I4MBUZ](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUZ?from=project-issue)|【新增规格】Swiper组件支持设置缓存cache|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|115|[I4MBV1](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV1?from=project-issue)|【新增规格】Image组件支持同步、异步渲染设置|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|116|[I4MBV3](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV3?from=project-issue)|【新增规格】样式设置特性增加组件多态样式设置规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|117|[I4MBV5](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV5?from=project-issue)|【新增规格】字母索引条组件增加提示菜单内容扩展规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|118|[I4MBV6](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV6?from=project-issue)|【新增规格】组件自定义特性增加自定义容器组件规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|119|[I4MBV7](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV7?from=project-issue)|【新增规格】滚动条样式自定义能力|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|120|[I4MBV8](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV8?from=project-issue)|【新增规格】Swiper组件新增切换禁用规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|121|[I4MBV9](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV9?from=project-issue)|【新增规格】Tabs组件新增TabBar内容自定义规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|122|[I4MBVA](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVA?from=project-issue)|【新增规格】Navigation组件新增标题栏设置规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|123|[I4MBVB](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVB?from=project-issue)|【新增规格】工具栏组件增加工具栏显隐控制规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|124|[I4MBVC](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVC?from=project-issue)|【新增规格】工具栏组件增加内容自定义能力规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|125|[I4MBVD](https://gitee.com/openharmony/interface_sdk-js/issues/I4MBVD?from=project-issue)|【新增特性】新增SysCap声明编译特性|标准系统|SIG_ApplicationFramework|[@karl-z](https://gitee.com/karl-z)|
|126|[I4MBVE](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVE?from=project-issue)|【新增特性】新增JS SDK编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|127|[I4MBVF](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVF?from=project-issue)|【新增特性】新增Config.json编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|128|[I4MBVG](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVG?from=project-issue)|【新增规格】新增断点调试特性支持单实例调试|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|129|[I4MBVH](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVH?from=project-issue)|【新增规格】新增attach调试特性支持单实例调试|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|130|[I4MBVI](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVI?from=project-issue)|【新增规格】新增声明式范式编译特性支持编译和校验规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|131|[I4MBVJ](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVJ?from=project-issue)|【新增特性】新增JS模块共享编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|132|[I4MBVK](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVK?from=project-issue)|【新增特性】新增HAR引用和编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|133|[I4MBVL](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVL?from=project-issue)|【新增特性】新增NPM引用和编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|134|[I4MBVM](https://gitee.com/openharmony/docs/issues/I4MBVM?from=project-issue)|【资料】ace_engine_standard部件IT2版本资料录入需求|标准系统|SIG_Docs|[@neeen](https://gitee.com/neeen)|
|135|[I4MBVN](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVN?from=project-issue)|【新增特性】纵向显示滑动条组件特性|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|136|[I4MBVO](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVO?from=project-issue)|【新增特性】Popup组件增加内容自定义规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|137|[I4MBVP](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVP?from=project-issue)|【新增特性】Canvas绘制能力支持|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|138|[I4MBVQ](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVQ?from=project-issue)|【新增规格】Canvas能力增强|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|139|[I4MBVR](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVR?from=project-issue)|【新增特性】触摸响应热区设置|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|140|[I4MBVS](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVS?from=project-issue)|【新增特性】Lottie动画支持|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|141|[I4MBVT](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVT?from=project-issue)|【新增特性】组件尺寸获取特性|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|142|[I4MBVU](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVU?from=project-issue)|【新增特性】Menu组件增加内容自定义规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|143|[I4MBVV](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVV?from=project-issue)|【新增特性】Swipe手势特性|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|144|[I4MBVW](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVW?from=project-issue)|【新增特性】UI预览支持Inspector能力|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|145|[I4MBVX](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVX?from=project-issue)|【新增特性】新增非路由文件预览特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|146|[I4MBVY](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVY?from=project-issue)|【新增特性】新增NAPI预览特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|147|[I4MBVZ](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVZ?from=project-issue)|【新增规格】新增声明式范式预览特性支持基础预览规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|148|[I4MBW2](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW2?from=project-issue)|【新增规格】新增声明式范式热加载特性支持已有节点修改规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|149|[I4MBW3](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW3?from=project-issue)|【新增规格】新增声明式范式热加载特性支持新增节点规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|150|[I4MBW4](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW4?from=project-issue)|【新增规格】新增声明式范式热加载特性支持删除节点规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|151|[I4MBW5](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW5?from=project-issue)|【新增规格】新增组件预览特性支持页面组件预览规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|152|[I4NY1T](https://gitee.com/openharmony/device_profile_core/issues/I4NY1T?from=project-issue)|【device_profile】订阅profile信息变化|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|153|[I4NY1U](https://gitee.com/openharmony/device_profile_core/issues/I4NY1U?from=project-issue)|【device_profile】订阅同步通知|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|154|[I4NY1V](https://gitee.com/openharmony/device_profile_core/issues/I4NY1V?from=project-issue)|【device_profile】CS采集OS特征信息|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|155|[I4NY1W](https://gitee.com/openharmony/device_profile_core/issues/I4NY1W?from=project-issue)|【device_profile】向业务端提供同步profile能力|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|156|[I4NY1X](https://gitee.com/openharmony/device_profile_core/issues/I4NY1X?from=project-issue)|【device_profile】提供查询远程设备profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|157|[I4NY1Z](https://gitee.com/openharmony/device_profile_core/issues/I4NY1Z?from=project-issue)|【device_profile】profile上线同步（wifi组网下）|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|158|[I4NY21](https://gitee.com/openharmony/device_profile_core/issues/I4NY21?from=project-issue)|【device_profile】提供删除本地profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|159|[I4NY22](https://gitee.com/openharmony/device_profile_core/issues/I4NY22?from=project-issue)|【device_profile】提供查询本地profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|160|[I4NY23](https://gitee.com/openharmony/device_profile_core/issues/I4NY23?from=project-issue)|【device_profile】提供写入profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|161|[I4MBRC](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRC?from=project-issue)|【hiperf部件】采样数据展示|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|162|[I4MBRD](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRD?from=project-issue)|【hiperf部件】性能数据采样记录|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|163|[I4MBRE](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRE?from=project-issue)|【hiperf部件】性能数据计数统计|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|164|[I4MBU1](https://gitee.com/openharmony/applications_settings/issues/I4MBU1?from=project-issue)|【设置公共数据存储】Settings数据管理API|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|165|[I4MBU8](https://gitee.com/openharmony/applications_settings/issues/I4MBU8?from=project-issue)|【设置】基础能力-多设备形态差异化构建|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|166|[I4MBU9](https://gitee.com/openharmony/applications_systemui/issues/I4MBU9?from=project-issue)|【SystemUI】【通知】通知组件化|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|

## OpenHarmony 3.1.2.3版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第3轮测试，验收:    |
| L0L1: http  2.0协议等                                        |
| L2: 驱动能力增强相关需求                                     |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:     http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165653/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165653-hispark_pegasus.tar.gz |
| hispark_taurus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165409/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165409-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165506/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165506-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2021-12-16**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：    http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.3/20211215_143812/version-Master_Version-Ohos_sdk_3.1.2.3-20211215_143812-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：    https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.2.3/20211215_150037/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_143541/version-Master_Version-OpenHarmony_3.1.2.3-20211215_143541-hispark_taurus_L2.tar.gz |
| RK3568版本:   http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211217_181409/version-Master_Version-OpenHarmony_3.1.2.3-20211217_181409-dayu200.tar.gz |

 

**需求列表:**

| no   | issue                                                        | feture    description                                        | platform | sig                | owner                                               |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- | ------------------ | --------------------------------------------------- |
| 1    | [I4KCO7](https://gitee.com/openharmony/build/issues/I4KCO7)  | [编译构建子系统]轻量级和标准系统支持使用统一的产品配置       | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)             |
| 2    | [I4JQ2N](https://gitee.com/openharmony/communication_netstack/issues/I4JQ2N) | [电话服务子系统]提供Http JS API                              | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 3    | [I4JQ3G](https://gitee.com/openharmony/third_party_nghttp2/issues/I4JQ3G) | [电话服务子系统]提供Http 2.0协议                             | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 4    | [I4LZZF](https://gitee.com/openharmony/drivers_framework/issues/I4LZZF) | [驱动子系统]支持同步/异步电源管理调用                        | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)             |
| 5    | [I4L3LF](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3LF) | [驱动子系统]传感器驱动模型能力增强                           | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)           |
| 6    | [I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR) | [驱动子系统]SR000GGUSG:【新增特性】Display-Layer HDI接口针对L2的参考实现； Display-Gralloc HDI接口针对L2的参考实现； Display-Device HDI接口针对L2的参考实现； | 标准系统 | SIG_Driver         | [@YUJIA](https://gitee.com/JasonYuJia)              |
| 7    | [I4D9V9](https://gitee.com/openharmony/drivers_framework/issues/I4D9V9) | [驱动子系统]hid类设备适配5.10内核                            | 标准系统 | SIG_Driver         | [@huangkai71](https://gitee.com/huangkai71)         |

## OpenHarmony 3.1.2.2版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.2**             |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第2轮测试，验收:    |
| L0L1: 支持Listeneer复读机                                    |
| L2: 分布式数据服务缺失功能补齐                               |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-10**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:     http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_100544/version-Master_Version-OpenHarmony_3.1.2.2-20211208_100544-hispark_pegasus.tar.gz |
| hispark_taurus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_105824/version-Master_Version-OpenHarmony_3.1.2.2-20211208_105824-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211210_092432/version-Master_Version-OpenHarmony_3.1.2.2-20211210_092432-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2021-12-10**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：    http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_093549/version-Master_Version-Ohos_sdk_3.1.2.2-20211208_093549-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：    https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_095201/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_141157/version-Master_Version-OpenHarmony_3.1.2.2-20211208_141157-hispark_taurus_L2.tar.gz |

 

**需求列表:**

| no   | issue                                                        | feture    description                              | platform | sig                | owner                                         |
| ---- | ------------------------------------------------------------ | -------------------------------------------------- | -------- | ------------------ | --------------------------------------------- |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机                | 轻量系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode)       |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode)       |
| 3    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ) | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒   | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)       |
| 4    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK) | [驱动子系统]传感器器件驱动能力增强                 | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)     |
| 5    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ) | [USB服务子系统]轻量级和标准系统使用统一的编译流程  | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen) |



## OpenHarmony 3.1.2.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.2.2              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第2轮测试，验收:|
|L0L1: 主要验收分布式数据对象需求                                           |
|L2: 主要验收编译构建和驱动子系统需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-12-10**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_105824/version-Master_Version-OpenHarmony_3.1.2.2-20211208_105824-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211210_092432/version-Master_Version-OpenHarmony_3.1.2.2-20211210_092432-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_100544/version-Master_Version-OpenHarmony_3.1.2.2-20211208_100544-hispark_pegasus.tar.gz |
| **L2转测试时间：2021-12-10**                                   |
 **L2转测试版本获取路径：**                                   |
 | hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_093549/version-Master_Version-Ohos_sdk_3.1.2.2-20211208_093549-ohos-sdk.tar.gz |
 | hi3516dv300-L2版本 SDK linxu/windows：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_141157/version-Master_Version-OpenHarmony_3.1.2.2-20211208_141157-hispark_taurus_L2.tar.gz|
 | hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_095201/L2-SDK-MAC.tar.gz|

需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机   | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode)             |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐       | 标准系统 | SIG_DataManagement  | [@widecode](https://gitee.com/widecode)         |
| 3    | [I4K7E3](https://gitee.com/openharmony/build/issues/I4K7E3)  | [编译构建子系统]支持使用统一的编译命令作为编译入口      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 4    | [I4KCMM](https://gitee.com/openharmony/build/issues/I4KCMM)  | [编译构建子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 5    | [I4KCNB](https://gitee.com/openharmony/build/issues/I4KCNB)  | [编译构建子系统]支持使用统一的gn模板                   | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 6    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ)  | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒      | 标准系统    | SIG_Driver  | [@fx_zhang](https://gitee.com/fx_zhang)         |
| 7    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK)  | [驱动子系统]传感器器件驱动能力增强               | 标准系统    | SIG_Driver  | [@Kevin-Lau](https://gitee.com/Kevin-Lau)         |
| 8    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ)  | [USB服务子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_Driver  | [@wu-chengwen](https://gitee.com/wu-chengwen)         |



## OpenHarmony 3.1.1.3版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.3              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第3轮测试，验收:|
|L0L1: 不涉及                                           |
|L2: 主要验收帐号及软总线相关需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L2转测试时间：2021-11-22**                                   |
| **L2转测试版本获取路径**                                   |
|hi3516dv300-L2版本 SDK linux/windows：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_093209/version-Master_Version-OpenHarmony_3.1.1.3-20211122_093209-ohos-sdk.tar.gz
| hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.1.3/20211122_094743/L2-SDK-MAC.tar.gz
| hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_090334/version-Master_Version-OpenHarmony_3.1.1.3-20211122_090334-hispark_taurus_L2.tar.gz|

需求列表：
| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------ | -------- | -------------- | ------------------------------------------- |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https//gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |


## OpenHarmony 3.1.1.3版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.3              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第3轮测试，验收:|
|L0L1: 不涉及                                          |
|L2: 主要验收帐号及软总线相关需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L2转测试时间：2021-11-22**                                   |
| **L2转测试版本获取路径**                                   |
|hi3516dv300-L2版本 SDK linux/windows：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_093209/version-Master_Version-OpenHarmony_3.1.1.3-20211122_093209-ohos-sdk.tar.gz
| hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.1.3/20211122_094743/L2-SDK-MAC.tar.gz
| hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_090334/version-Master_Version-OpenHarmony_3.1.1.3-20211122_090334-hispark_taurus_L2.tar.gz|

需求列表：
| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------ | -------- | -------------- | ------------------------------------------- |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https//gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |


## OpenHarmony 3.1.1.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.2               |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第一轮测试，验收:|
|L0L1:HiStreamer相关需求，性能优化及linux版本热插拔                                            |
|L2: DFX      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-11-15**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.2/20211110_150708/version-Master_Version-OpenHarmony_3.1.1.2-20211110_150708-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.2/20211110_151619/version-Master_Version-OpenHarmony_3.1.1.2-20211110_151619-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony__3.1.1.2/20211110_151211/version-Master_Version-OpenHarmony_3.1.1.2-20211110_151211-hispark_pegasus.tar.gz |

需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I4DK89](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK89) | 【需求】HiStreamer插件框架需求                             | 轻量系统 | SIG_GraphicsandMedia| [@guodongchen](https://gitee.com/guodongchen)  |
| 2   | [I4DK8D](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK8D) | 【需求】HiStreamer性能和DFX需求                            | 轻量系统 | SIG_GraphicsandMedia| [@guodongchen](https://gitee.com/guodongchen)  |
| 3   | [I3ND6Y](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ND6Y) | 【性能】OS内核&驱动启动优化                            | 轻量系统 | SIG_Kernel| [@kkup180](https://gitee.com/kkup180) |
| 4   | [I3NTCT](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTCT) | Linux版本init支持热插拔                            | 轻量系统 | SIG_BscSoftSrv| [@handyohos](https://gitee.com/handyohos) |


## OpenHarmony 3.1.0.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.0.2               |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第七轮测试，验收:|
|L0L1:无                                              |
|L2: DFX      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-10-28**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_093735/version-Master_Version-OpenHarmony_3.1.0.2-20211027_093735-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094506/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094506-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094100/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094100-hispark_pegasus.tar.gz |
| **L2转测试时间：2021-10-28**                                   |
 **L2转测试版本获取路径：**                                   |
 | hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094329/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094329-hispark_taurus_L2.tar.gz |
 | hi3516dv300-L2版本 SDK linxu/windows：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211028_124231/version-Master_Version-OpenHarmony_3.1.0.2-20211028_124231-ohos-sdk.tar.gz |
 | hi3516dv300-L2版本 SDK mac：<br> https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.0.2/20211028_194502/L2-SDK-MAC.tar.gz|


需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I3XGJH](https://gitee.com/openharmony/startup_init_lite/issues/I3XGJH) | init基础环境构建                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 2   | [I3XGKV](https://gitee.com/openharmony/startup_init_lite/issues/I3XGKV) | sytemparameter管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 3   | [I3XGLN](https://gitee.com/openharmony/startup_init_lite/issues/I3XGLN) | init 脚本管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 4   | [I3XGM3](https://gitee.com/openharmony/startup_init_lite/issues/I3XGM3) | init 服务管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 5   | [I3XGMQ](https://gitee.com/openharmony/startup_init_lite/issues/I3XGMQ) | 基础权限管理                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 6   | [I3XGN8](https://gitee.com/openharmony/startup_init_lite/issues/I3XGN8) | bootimage构建和加载                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 7   | [I3XGO7](https://gitee.com/openharmony/startup_init_lite/issues/I3XGO7) | uevent 管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 8   | [I4BX5Z](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX5Z) | 【需求】HiStreamer支持音频播放和控制             | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 9   | [I4BX8A](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX8A) | 【需求】HiStreamer支持常见音频格式mp3/wav的播放   | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 10   | [I4BX9E](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX9E) | 【需求】HiStreamer播放引擎框架需求               | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
